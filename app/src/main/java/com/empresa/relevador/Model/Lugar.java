package com.empresa.relevador.Model;

/**
 * Created by fermenendez on 26/8/15.
 */
public class Lugar {

    private Integer id;
    private String tipo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}

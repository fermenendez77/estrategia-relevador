package com.empresa.relevador.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.empresa.relevador.Controller.DAO.RubroDAO;
import com.empresa.relevador.Model.Rubro;
import com.empresa.relevador.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by fermenendez on 9/9/15.
 */
public class RubrosAdapter extends BaseAdapter {

    private List<Rubro> RubroList;
    private Context mContext;
    private RubroDAO rubroDAO;
    private int selectedIndex;

    public RubrosAdapter(Context context){

        RubroList = new ArrayList<Rubro>();
        this.rubroDAO = RubroDAO.getInstance(context);
        this.mContext = context;
        this.selectedIndex = -1;
    }
    @Override
    public int getCount() {
        return RubroList.size();
    }

    @Override
    public Rubro getItem(int position) {
        return RubroList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return RubroList.get(position).getId() ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.adapter_marca,null);
            holder.nameTextView = (TextView) view.findViewById(R.id.textInputName);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }
        if(selectedIndex!= -1 && position == selectedIndex) {
            holder.nameTextView.setBackgroundColor(mContext.getResources().getColor(R.color.greenButton));
            holder.nameTextView.setTextColor(mContext.getResources().getColor(R.color.lightGreen));
        }
        else {
            holder.nameTextView.setBackgroundColor(mContext.getResources().getColor(R.color.lightGreen));
            holder.nameTextView.setTextColor(mContext.getResources().getColor(R.color.greenButton));
        }

        holder.nameTextView.setText(RubroList.get(position).getNombre());
        return view;
    }
    private static class ViewHolder{

        TextView nameTextView;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
        notifyDataSetChanged();
    }

    public List<Rubro> getRubroList() {
        return RubroList;
    }

    public int getPosition(Rubro rubro){

        int pos = 0;
        for(Rubro r: RubroList){

            if(r.getId() == rubro.getId()){

                return pos;
            }
            else{

                pos++;
            }
        }
        return -1;
    }

    public void setRubroList(List<Rubro> rubroList) {
        RubroList = rubroList;
    }
}

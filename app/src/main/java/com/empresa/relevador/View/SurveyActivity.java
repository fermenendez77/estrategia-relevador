package com.empresa.relevador.View;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.empresa.relevador.Controller.BO.CalleBO;
import com.empresa.relevador.Controller.DAO.LugarDAO;
import com.empresa.relevador.Controller.DAO.MarcaDAO;
import com.empresa.relevador.Controller.DAO.RelevamientoDAO;
import com.empresa.relevador.Model.Calle;
import com.empresa.relevador.Model.Lugar;
import com.empresa.relevador.Model.Marca;
import com.empresa.relevador.Model.Paridad;
import com.empresa.relevador.Model.Relevamiento;
import com.empresa.relevador.Model.Tipo;
import com.empresa.relevador.R;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

public class SurveyActivity extends Activity implements SurveyTypeDialog.OnCompleteListener {

    private TextView streetName;
    private ImageButton nextNumber;
    private ImageButton backNumber;
    private TextView numberStreet;
    private ListView imparListView;
    private ListView parListView;
    private LinearLayout surveyTable;
    private LinearLayout parLayout;
    private LinearLayout imparLayout;
    private ImageButton imparBtn;
    private ImageButton parBtn;
    private SurveyAdapter parAdapter;
    private SurveyAdapter imparAdapter;
    private int paridad;
    private SurveyTypeDialog typeDialog;
    private Calle calle;
    private CalleBO calleBO;
    private String orientacion;
    private Integer numCalle;
    private RelevamientoDAO relevamientoDAO;
    private Integer lastPosPar;
    private Integer lastPosImpar;
    private List<Relevamiento> listPar;
    private List<Relevamiento> listImpar;
    private Context mContext;
    private Integer position;
    private MarcaDAO marcaDAO;
    private Marca selectedMarca;
    private LugarDAO lugarDAO;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
            mContext = this;
            streetName = (TextView) findViewById(R.id.streetNameTextView);
            nextNumber = (ImageButton) findViewById(R.id.nextNumberSurveyBtn);
            backNumber = (ImageButton) findViewById(R.id.backNumberSurveyBtn);
            numberStreet = (TextView) findViewById(R.id.numberStreetTextView);
            imparListView = (ListView) findViewById(R.id.imparListView);
            parListView = (ListView) findViewById(R.id.parListView);
            surveyTable = (LinearLayout) findViewById(R.id.surveyTable);
            parLayout = (LinearLayout) findViewById(R.id.parLayaut);
            imparLayout = (LinearLayout) findViewById(R.id.imparLayaut);
            imparBtn = (ImageButton) findViewById(R.id.addImparBtn);
            parBtn = (ImageButton) findViewById(R.id.addParBtn);
            calleBO = CalleBO.getInstance(this);
            marcaDAO = MarcaDAO.getInstance(this);
            relevamientoDAO = RelevamientoDAO.getInstance(this);
            lugarDAO = LugarDAO.getInstance(this);
            calle = calleBO.findById(this.getIntent().getIntExtra("idCalle", 0));
            orientacion = this.getIntent().getStringExtra("orientacion");
            numCalle = this.getIntent().getIntExtra("numCalle",0);
            if(orientacion.equals("Descendente")){

                surveyTable.removeAllViews();
                surveyTable.addView(parLayout);
                surveyTable.addView(imparLayout);
            }
            streetName.setText(calle.getNombre());
            numberStreet.setText(numCalle + " - " + (numCalle + 100));
            listPar = relevamientoDAO.findByStreet(calle.getId(),numCalle,Paridad.PAR);
            listImpar = relevamientoDAO.findByStreet(calle.getId(),numCalle,Paridad.IMPAR);
            parAdapter = new SurveyAdapter(getApplicationContext(),listPar);
            imparAdapter = new SurveyAdapter(getApplicationContext(),listImpar);
            lastPosPar = listPar.size()==0? 0 : listPar.get(listPar.size() - 1 ).getPosicion();
            lastPosImpar = listImpar.size()==0? 0: listImpar.get(listImpar.size() - 1 ).getPosicion();
            imparListView.setAdapter(imparAdapter);
            parListView.setAdapter(parAdapter);
            typeDialog = new SurveyTypeDialog();

        //Buttons

            nextNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(numCalle<calle.getMaxNum()) {
                        numCalle += 100;
                        numberStreet.setText(numCalle + " - " + (numCalle + 100));
                        listImpar = relevamientoDAO.findByStreet(calle.getId(),numCalle,Paridad.IMPAR);
                        listPar = relevamientoDAO.findByStreet(calle.getId(),numCalle,Paridad.PAR );
                        lastPosPar = listPar.size()==0? 0 : listPar.get(listPar.size() - 1 ).getPosicion();
                        lastPosImpar = listImpar.size()==0? 0: listImpar.get(listImpar.size() - 1 ).getPosicion();
                        parAdapter.setRelevamientos(listPar);
                        imparAdapter.setRelevamientos(listImpar);
                        parAdapter.notifyDataSetChanged();
                        imparAdapter.notifyDataSetChanged();
                    }
                }
            });
            backNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(numCalle>0) {
                        numCalle-=100;
                        numberStreet.setText(numCalle + " - " + (numCalle + 100));
                        listImpar = relevamientoDAO.findByStreet(calle.getId(),numCalle,Paridad.IMPAR);
                        listPar = relevamientoDAO.findByStreet(calle.getId(),numCalle,Paridad.PAR );
                        lastPosPar = listPar.size()==0? 0 : listPar.get(listPar.size() - 1 ).getPosicion();
                        lastPosImpar = listImpar.size()==0? 0: listImpar.get(listImpar.size() - 1 ).getPosicion();
                        parAdapter.setRelevamientos(listPar);
                        imparAdapter.setRelevamientos(listImpar);
                        parAdapter.notifyDataSetChanged();
                        imparAdapter.notifyDataSetChanged();

                    }
                }
            });
            parBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    paridad = 1;
                    if(notInsertDialog(Paridad.PAR))
                        typeDialog.show(getFragmentManager(), null);

                }
            });
            imparBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    paridad = 0;
                    if(notInsertDialog(Paridad.IMPAR))
                        typeDialog.show(getFragmentManager(),null);
                }
            });

            imparListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    editPosition(Paridad.IMPAR, position, imparAdapter.getCount());
                    return true;
                }
            });

            parListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    editPosition(Paridad.PAR, position, parAdapter.getCount());
                    return true;

                }
            });


            imparListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                    abmDialog(Paridad.IMPAR, position);
                }
            });

            parListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    abmDialog(Paridad.PAR, position);
                }
            });


    }
    //Callback del DialogFragment
    public void onComplete(final Tipo result){

        final SurveyAdapter adapter;
        final Relevamiento relevamiento = new Relevamiento();
        final int pos;

        if(paridad == 1){ //Par

            adapter = parAdapter;
            relevamiento.setParidad(Paridad.PAR);
            pos = lastPosPar;
        }
        else {  //impar

            adapter = imparAdapter;
            relevamiento.setParidad(Paridad.IMPAR);
            pos = lastPosImpar;
        }

        Lugar lugar;
        if (result!=Tipo.LOCAL_OCUPADO) {

            lugar = lugarDAO.findById(result.getNumVal());
            relevamiento.setLugar(lugar);
            insertRelevamiento(relevamiento, null, adapter);
        }
        else{
            Intent i = new Intent(SurveyActivity.this, OccupedSiteActivity.class)
                        .putExtra("idCalle", calle.getId())
                        .putExtra("orientacion", orientacion)
                        .putExtra("numCalle", numCalle)
                        .putExtra("paridad", paridad)
                        .putExtra("posicion", pos+1);
            startActivity(i);
        }
    }

    private void insertRelevamiento(Relevamiento relevamiento, Marca marca, SurveyAdapter adapter){

        if(relevamiento.getParidad() == Paridad.IMPAR) {

            lastPosImpar+=1;
            relevamiento.setPosicion(lastPosImpar);
        }else{

            lastPosPar+=1;
            relevamiento.setPosicion(lastPosPar);
        }
        relevamiento.setOrientacion(orientacion);
        relevamiento.setCalle(calle);
        relevamiento.setMarca(marca);
        relevamiento.setNumeracion(numCalle);
        long id = relevamientoDAO.insert(relevamiento);
        relevamiento.setId(id);
        adapter.getRelevamientos().add(relevamiento);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(SurveyActivity.this,NewEntryActivity.class);
        startActivity(i);
    }

    private void abmDialog(final Paridad parity,final int position){

          AlertDialog.Builder dialog = new AlertDialog.Builder(SurveyActivity.this);
                    dialog.setTitle("Editar Campo");
                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                            SurveyActivity.this,
                            android.R.layout.select_dialog_item
                    );
                    arrayAdapter.add("Modificar");
                    arrayAdapter.add("Eliminar");
                    dialog.setAdapter(arrayAdapter,new DialogInterface.OnClickListener(){

                        SurveyAdapter adapter = (parity==Paridad.PAR)?parAdapter:imparAdapter;

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            switch (which) {
                                case 0:
                                    Intent i = new Intent(SurveyActivity.this,ModifyActivity.class);
                                    i.putExtra("idRelevamiento",adapter.getRelevamientos().get(position).getId())
                                            .putExtra("idCalle", calle.getId())
                                            .putExtra("orientacion", orientacion)
                                            .putExtra("numCalle", numCalle)
                                            .putExtra("paridad", paridad)
                                            .putExtra("posicion", position);
                                    startActivity(i);
                                    break;
                                case 1:

                                    deleteRelevamiento(position,adapter);
                                    break;
                                }
                        }
                    });
                    dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
    }

    private void editPosition(Paridad parity, final int position, int count){


        final SurveyAdapter adapter = (parity==Paridad.PAR)?parAdapter:imparAdapter;
        final NumberPicker picker = new NumberPicker(mContext);
        picker.setMaxValue(count);
        picker.setMinValue(1);
        picker.setValue(position + 1);
        AlertDialog.Builder dialog = new AlertDialog.Builder(SurveyActivity.this);
        dialog.setTitle("Editar Posición de unidad relevada");
        dialog.setMessage("Posicion a Reemplazar: " + Integer.toString(position + 1));
        dialog.setView(picker);
        dialog.setNegativeButton("Cancelar", null);
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                int selectedPosition = position;
                int changedPosition = picker.getValue() - 1;

                Relevamiento selectedRelevamiento = adapter.getItem(selectedPosition);
                Relevamiento changedRelevamiento = adapter.getItem(changedPosition);

                selectedRelevamiento.setPosicion(changedPosition + 1);
                changedRelevamiento.setPosicion(selectedPosition + 1);

                //Update AdapterView Positions
                adapter.getRelevamientos().set(selectedPosition, changedRelevamiento);
                adapter.getRelevamientos().set(changedPosition, selectedRelevamiento);
                //Update on BD
                relevamientoDAO.update(selectedRelevamiento.getId(), selectedRelevamiento);
                relevamientoDAO.update(changedRelevamiento.getId(), changedRelevamiento);

                adapter.notifyDataSetChanged();
            }
        });
        dialog.show();
    }

    private void deleteRelevamiento(int position, SurveyAdapter adapter){

        int r = relevamientoDAO.delete(adapter.getRelevamientos().get(position));

        if(r >0) {

            if(adapter.equals(imparAdapter))
                lastPosImpar-=1;
            else
                lastPosPar-=1;
            ListIterator<Relevamiento> listIterator = adapter.getRelevamientos().listIterator(position+1);
            while(listIterator.hasNext()){

                Relevamiento relevamiento = listIterator.next();
                int newpos = relevamiento.getPosicion()-1;
                relevamiento.setPosicion(newpos);
                int res = relevamientoDAO.update(relevamiento.getId(),relevamiento);
            }
            adapter.getRelevamientos().remove(position);
            adapter.notifyDataSetChanged();
        }
    }

    private boolean notInsertDialog(Paridad parity){


        String parityString = (parity==Paridad.PAR)?"Par":"Impar";
        final SurveyAdapter adapter = (parity==Paridad.PAR)?parAdapter:imparAdapter;
        if(!adapter.isEmpty()){

            if(adapter.getItem(0).getOrientacion().equals(orientacion) ) {

                return true;
            }
            else {
                AlertDialog.Builder dialog = new AlertDialog.Builder(SurveyActivity.this);
                dialog.setTitle("Orientación inversa");
                dialog.setMessage("Ya existen datos cargados en la orientación opuesta para " + calle.getNombre() + " en la altura "
                        + numCalle + " del lado " + parityString + ".Regrese al menú anterior y cambie la Orientación");
                dialog.setPositiveButton("Aceptar", null);
                dialog.show();
                return false;
            }
        }

        return true;
    }
}

package com.empresa.relevador.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.empresa.relevador.Model.SubRubro;
import com.empresa.relevador.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fermenendez on 8/9/15.
 */
public class SubRubAdapter extends BaseAdapter {

    private List<SubRubro> subRubroList;
    private Context mContext;
    private int selectedIndex;
    public SubRubAdapter(Context context){

        subRubroList = new ArrayList<SubRubro>();
        this.selectedIndex = -1;
        this.mContext = context;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
       return subRubroList.size();
    }

    @Override
    public SubRubro getItem(int position) {
       return subRubroList.get(position);
    }

    @Override
    public long getItemId(int position) {
       return subRubroList.get(position).getId() ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.adapter_subrub,null);
            holder.nameTextView = (TextView) view.findViewById(R.id.textInputName);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }

        if(selectedIndex!= -1 && position == selectedIndex) {
            holder.nameTextView.setBackgroundColor(mContext.getResources().getColor(R.color.greenButton));
            holder.nameTextView.setTextColor(mContext.getResources().getColor(R.color.lightGreen));
        }
        else {
            holder.nameTextView.setBackgroundColor(mContext.getResources().getColor(R.color.lightGreen));
            holder.nameTextView.setTextColor(mContext.getResources().getColor(R.color.greenButton));
        }
        holder.nameTextView.setText(subRubroList.get(position).getNombre());
        return view;
    }
    private static class ViewHolder{

        TextView nameTextView;
    }

    public List<SubRubro> getSubRubroList() {
        return subRubroList;
    }

    public void setSubRubroList(List<SubRubro> subRubroList) {
        this.subRubroList = subRubroList;
    }

    public int getPosition(SubRubro subRubro){

        int pos = 0;
        for(SubRubro s: subRubroList){

            if(s.getId() == subRubro.getId()){

                return pos;
            }
            else{

                pos++;
            }
        }
        return -1;
    }
}

package com.empresa.relevador.View;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.empresa.relevador.Controller.DAO.LugarDAO;
import com.empresa.relevador.Controller.DAO.MarcaDAO;
import com.empresa.relevador.Controller.DAO.RelevamientoDAO;
import com.empresa.relevador.Model.Marca;
import com.empresa.relevador.Model.Relevamiento;
import com.empresa.relevador.Model.Tipo;
import com.empresa.relevador.R;

import org.w3c.dom.Text;

public class ModifyActivity extends Activity {

    private Relevamiento selectedRelevamiento;
    private Relevamiento changeRelevamiento;
    private RelevamientoDAO relevamientoDAO;
    private LinearLayout marcaLayaut;
    private Button apllyChangesBtn;
    private LugarDAO lugarDAO;
    private Spinner typeSpinner;
    private Intent recIntent;
    private TextView marcaTextView;
    private TextView rubroTextView;
    private TextView subRubroTextView;
    private Button changeMarcaBtn;
    private MarcaDAO marcaDAO;
    private Context mContext;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify);
        mContext = this;
        recIntent = getIntent();
        lugarDAO = LugarDAO.getInstance(this);
        marcaDAO = MarcaDAO.getInstance(this);
        relevamientoDAO = RelevamientoDAO.getInstance(this);
        long idMarca = recIntent.getLongExtra("idMarca",0);
        selectedRelevamiento = relevamientoDAO.findById(getIntent().getLongExtra("idRelevamiento", 0));
        changeRelevamiento = selectedRelevamiento;
        if(idMarca!=0){

            changeRelevamiento.setLugar(lugarDAO.findById(Tipo.LOCAL_OCUPADO.getNumVal()));
        }
        typeSpinner = (Spinner) findViewById(R.id.typeSpinner);
        apllyChangesBtn = (Button) findViewById(R.id.applyChangesBtn);
        marcaLayaut = (LinearLayout) findViewById(R.id.marcasLinearLayout);
        marcaTextView = (TextView) findViewById(R.id.marcaTextView);
        rubroTextView = (TextView) findViewById(R.id.rubroTextView);
        changeMarcaBtn = (Button) findViewById(R.id.changeMarcaBtn);
        subRubroTextView = (TextView) findViewById(R.id.subRubroTextView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, lugarDAO.getAllArray());
        typeSpinner.setAdapter(adapter);
        typeSpinner.getBackground().setColorFilter(getResources().getColor(R.color.blueGoogle), PorterDuff.Mode.SRC_ATOP);
        if(changeRelevamiento.getLugar().getId()!=4){

            marcaLayaut.setVisibility(View.INVISIBLE);
        }else{

            if(idMarca!=0){

                Marca marca = marcaDAO.findbyId(idMarca);
                changeRelevamiento.setMarca(marca);
            }
            marcaTextView.setText(changeRelevamiento.getMarca().getNombre());
            rubroTextView.setText(changeRelevamiento.getMarca().getRubro().getNombre());
            subRubroTextView.setText((changeRelevamiento.getMarca().getSubRubro() == null) ? "" : selectedRelevamiento.getMarca().getSubRubro().getNombre());
        }
        int pos = adapter.getPosition(changeRelevamiento.getLugar().getTipo());
        typeSpinner.setSelection(pos);
        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //      Log.d("Tipo",typeSpinner.getSelectedItem().toString());
                changeRelevamiento.setLugar(lugarDAO.getHashMap().get(typeSpinner.getSelectedItem().toString()));
                if (changeRelevamiento.getLugar().getId() == 4) {

                    marcaLayaut.setVisibility(View.VISIBLE);
                } else
                    marcaLayaut.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        apllyChangesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            if( (changeRelevamiento.getLugar().getId() == Tipo.LOCAL_OCUPADO.getNumVal()) && changeRelevamiento.getMarca() == null){

                 Toast.makeText(mContext,"Debe definir la marca mediante el Boton Modificar Marca",Toast.LENGTH_LONG).show();

            }
            else{
                relevamientoDAO.update(selectedRelevamiento.getId(),changeRelevamiento);
                Intent intent = new Intent(ModifyActivity.this, SurveyActivity.class);
                intent.putExtra("idCalle", recIntent.getIntExtra("idCalle", 0))
                        .putExtra("orientacion", recIntent.getStringExtra("orientacion"))
                        .putExtra("numCalle", recIntent.getIntExtra("numCalle", 0))
                        .putExtra("paridad", recIntent.getIntExtra("paridad", 0))
                        .putExtra("posicion", recIntent.getIntExtra("position", 0));
                startActivity(intent);
                  }
            }
        });
        changeMarcaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ModifyActivity.this,OccupedSiteActivity.class);
                intent.putExtra("idRelevamiento",selectedRelevamiento.getId());
                intent.putExtra("idCalle", recIntent.getIntExtra("idCalle", 0))
                        .putExtra("orientacion", recIntent.getStringExtra("orientacion"))
                        .putExtra("numCalle", recIntent.getIntExtra("numCalle", 0))
                        .putExtra("paridad", recIntent.getIntExtra("paridad", 0))
                        .putExtra("posicion", recIntent.getIntExtra("position", 0));
                startActivity(intent);
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(ModifyActivity.this,SurveyActivity.class);
        intent.putExtra("idCalle", recIntent.getIntExtra("idCalle", 0))
                .putExtra("orientacion", recIntent.getStringExtra("orientacion"))
                .putExtra("numCalle", recIntent.getIntExtra("numCalle", 0))
                .putExtra("paridad", recIntent.getIntExtra("paridad", 0))
                .putExtra("posicion", recIntent.getIntExtra("position", 0));
        startActivity(intent);
    }


}

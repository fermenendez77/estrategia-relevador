package com.empresa.relevador.View;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.empresa.relevador.R;

public class RubrosActivity extends Activity {

    private ListView rubrosListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rubros);

        rubrosListView = (ListView) findViewById(R.id.rubrosList);
        final RubrosAdapter adapter = new RubrosAdapter(this);
        rubrosListView.setAdapter(adapter);
        final Intent recIntent = this.getIntent();
        rubrosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i = new Intent(RubrosActivity.this,OccupedSiteActivity.class)
                        .putExtra("idCalle",recIntent.getIntExtra("idCalle",0))
                        .putExtra("orientacion", recIntent.getStringExtra("orientacion"))
                        .putExtra("numCalle", recIntent.getIntExtra("numCalle", 0))
                        .putExtra("rubroId",adapter.getItem(position).getId() )
                        .putExtra("paridad",recIntent.getIntExtra("paridad",0))
                        .putExtra("posicion",recIntent.getIntExtra("posicion",0));

                startActivity(i);
            }
        });
    }


}

package com.empresa.relevador.View;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.empresa.relevador.Controller.BO.CalleBO;
import com.empresa.relevador.Model.Calle;
import com.empresa.relevador.R;

public class NewEntryActivity extends Activity {

    private Spinner orientationSpinner;
    private AutoCompleteTextView streetInput;
    private Button initSurveyBtn;
    private ImageButton backNumberBtn;
    private ImageButton nextNumberBtn;
    private TextView numberTextView;
    private String selectedStreet = "asasasakjdkjkdjfkdjfkd";
    private CalleBO calleBO;
    private Integer numCalle = 0;
    private Integer maxnumCalle = 0;
    private Integer idCalle;
    private Context mContext;
    private InitAsync async;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_entry);
        mContext = this;
        streetInput = (AutoCompleteTextView) findViewById(R.id.streetInput);
        orientationSpinner = (Spinner) findViewById(R.id.orientationSpinner);
        initSurveyBtn = (Button) findViewById(R.id.initSurveyBtn);
        backNumberBtn = (ImageButton) findViewById(R.id.backNumberBtn);
        nextNumberBtn = (ImageButton) findViewById(R.id.nextNumberBtn);
        numberTextView = (TextView) findViewById(R.id.numberTextView);
        calleBO = CalleBO.getInstance(this);
        //Cambia el color del triangulito:
        orientationSpinner.getBackground().setColorFilter(getResources().getColor(R.color.blueGoogle), PorterDuff.Mode.SRC_ATOP);
        async = new InitAsync();
        async.execute();
        //Adapters
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.orientationStrings, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        orientationSpinner.setAdapter(adapter);
        streetInput.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedStreet = streetInput.getText().toString();
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromInputMethod(view.getWindowToken(),0);
                in.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                Log.i("Calle id:",String.valueOf(calleBO.getMapCalle().get(selectedStreet).getId() ) );
                idCalle = calleBO.getMapCalle().get(selectedStreet).getId();
                maxnumCalle = calleBO.getMapCalle().get(selectedStreet).getMaxNum();
                numCalle = 0;
                numberTextView.setText(numCalle+" - "+(numCalle+100));

            }
        });
        numberTextView.setText(numCalle+" - "+(numCalle+100));
        //Buttons
        initSurveyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(streetInput.getText().toString().length() == 0) {
                    streetInput.setError("Debe ingresar una calle a Relevar");
                }
                else if(!selectedStreet.equals(streetInput.getText().toString())){
                    streetInput.setError("La calle ingresada no existe, debe seleccionar una de las calles sugeridas");
                }
                else{

                    String orientacion = orientationSpinner.getSelectedItem().toString();
                    Intent i = new Intent(NewEntryActivity.this, SurveyActivity.class)
                            .putExtra("idCalle",idCalle)
                            .putExtra("orientacion",orientacion)
                            .putExtra("numCalle",numCalle);
                    startActivity(i);
                }
            }
        });
        backNumberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(numCalle>0){
                    numCalle-=100;
                    numberTextView.setText(numCalle+" - "+(numCalle+100));
                }
            }
        });
        nextNumberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(numCalle<maxnumCalle){
                    numCalle += 100;
                    numberTextView.setText(numCalle + " - " + (numCalle + 100));
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(NewEntryActivity.this,MainActivity.class);
        startActivity(i);
    }

    private class InitAsync extends AsyncTask<Void,Void,Boolean>{
        private String[] callesArray;
        private ProgressDialog pDialog;
        @Override
        protected Boolean doInBackground(Void... params) {
            callesArray = calleBO.getAllArray();
            return true;
        }

        @Override
        protected void onPreExecute() {

            pDialog = ProgressDialog.show(mContext, "Espere un momento", "Cargando Información");

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            ArrayAdapter<String> adapterStreet = new ArrayAdapter<String>(mContext,
                    android.R.layout.simple_dropdown_item_1line, callesArray);
            streetInput.setAdapter(adapterStreet);
            pDialog.dismiss();
        }
    }
}

package com.empresa.relevador.View;

import android.content.Context;
import android.provider.BaseColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.empresa.relevador.Controller.BO.CalleBO;
import com.empresa.relevador.Model.Calle;
import com.empresa.relevador.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fermenendez on 5/9/15.
 */
public class StreetCustomAdapter extends ArrayAdapter<Calle> {

    private Context mContext;
    private List<Calle> calles;
    private List<Calle> suggestions;
    CalleBO calleBO;
    public StreetCustomAdapter(Context context, int viewResourceId){

        super(context,viewResourceId);
        calleBO = CalleBO.getInstance(context);
        this.calles = new ArrayList<Calle>();
        this.mContext = context;
        this.calles = calleBO.getAll(context);
        suggestions = new ArrayList<Calle>();

    }

    public int getCount() {
        return calles.size();
    }

    public Calle getItem(int position) {
        return calles.get(position);
    }

    public long getItemId(int position) {
        return calles.get(position).getId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view == null) {

            holder = new ViewHolder();
            view = inflater.inflate(R.layout.calles_adapter, null);
            holder.street = (TextView) view.findViewById(R.id.streetName);
            view.setTag(holder);
        }
        else{

            holder = (ViewHolder) view.getTag();
        }
        holder.street.setText(calles.get(position).getNombre());
        return view;
    }

    private static class ViewHolder {

        TextView street;
    }


    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            String str = ((Calle)(resultValue)).getNombre();
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (Calle calle : calles) {
                    if(calle.getNombre().toLowerCase().startsWith(constraint.toString().toLowerCase())){
                        suggestions.add(calle);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Calle> filteredList = (ArrayList<Calle>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (Calle c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };
}

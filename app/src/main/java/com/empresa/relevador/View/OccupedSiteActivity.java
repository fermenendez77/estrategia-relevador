package com.empresa.relevador.View;

import android.app.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import com.empresa.relevador.Controller.DAO.MarcaDAO;
import com.empresa.relevador.Controller.DAO.RelevamientoDAO;
import com.empresa.relevador.Controller.DAO.RubroDAO;
import com.empresa.relevador.Controller.DAO.SubRubroDAO;
import com.empresa.relevador.Model.Calle;
import com.empresa.relevador.Model.Lugar;
import com.empresa.relevador.Model.Marca;
import com.empresa.relevador.Model.Paridad;
import com.empresa.relevador.Model.Relevamiento;
import com.empresa.relevador.Model.Rubro;
import com.empresa.relevador.Model.SubRubro;
import com.empresa.relevador.Model.Tipo;
import com.empresa.relevador.R;
import java.util.List;


public class OccupedSiteActivity extends Activity {

    private AutoCompleteTextView marcaAutoTextView;
    private ListView subRubroListView;
    private ListView marcaListView;
    private Button addMarcaBtn;
    //private Button unkownBtn;
    private RubroDAO rubroDAO;
    private SubRubroDAO subRubroDAO;
    private MarcaDAO marcaDAO;
    private Rubro rubroSelected;
    private SubRubro subRubroSelected;
    private Intent intent;
    private RelevamientoDAO relevamientoDAO;
    private List<Marca> marcaList;
    private Context context;
    private SubRubAdapter subRubAdapter;
    private MarcaAdapter marcaAdapter;
    private ArrayAdapter<String>  adapterRubros;
    private Marca selectedMarca;
    private Spinner rubroSpinner;
    private String[] marcas;
    private String[] rubros;
    private boolean autoCall;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_occuped_site);
        intent = this.getIntent();
        autoCall = false;
        marcaAutoTextView = (AutoCompleteTextView) findViewById(R.id.marcaAutoInput);
        subRubroListView = (ListView) findViewById(R.id.subRubroListView);
        marcaListView = (ListView) findViewById(R.id.marcaListView);
        addMarcaBtn = (Button) findViewById(R.id.addMarcaBtn);
        rubroSpinner = (Spinner) findViewById(R.id.rubroSpinner);
        rubroSpinner.getBackground().setColorFilter(getResources().getColor(R.color.blueGoogle), PorterDuff.Mode.SRC_ATOP);
        //unkownBtn = (Button) findViewById(R.id.unkownMarca);
        context = this;
        rubroDAO = RubroDAO.getInstance(this);
        marcaDAO = MarcaDAO.getInstance(this);
        subRubroDAO = SubRubroDAO.getInstance(this);
        relevamientoDAO = RelevamientoDAO.getInstance(this);
        InitAsync async = new InitAsync(this);
        async.execute();
        //Adapters
        addMarcaBtn.setBackgroundColor(getResources().getColor(R.color.grey));
        marcaAutoTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                autoCall = true;
                selectedMarca = marcaDAO.getHashMap().get(marcaAutoTextView.getText().toString());
                rubroSelected = selectedMarca.getRubro();
                subRubroSelected = selectedMarca.getSubRubro();
                subRubAdapter.setSubRubroList(subRubroDAO.getSubRubros(rubroSelected));
                int pos = subRubAdapter.getPosition(subRubroSelected);
                subRubAdapter.setSelectedIndex(pos);
                subRubroListView.smoothScrollToPosition(pos);
                subRubAdapter.setSubRubroList(subRubroDAO.getSubRubros(rubroSelected));
                subRubAdapter.notifyDataSetChanged();
                marcaAdapter.setMarcaList(marcaDAO.findBySubRubro(subRubroSelected));
                int posSub = marcaAdapter.getPosition(selectedMarca);
                marcaAdapter.setSelectedIndex(posSub);
                marcaListView.smoothScrollToPosition(posSub);
                addMarcaBtn.setBackgroundColor(getResources().getColor(R.color.greenButton));
                addMarcaBtn.setText("SELECCIONAR MARCA : " + selectedMarca.getNombre());
                int posRub = adapterRubros.getPosition(rubroSelected.getNombre());
                rubroSpinner.setSelection(posRub);
                //Esconder teclado:
                InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromInputMethod(view.getWindowToken(), 0);
                in.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        });
        marcaListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectedMarca = marcaAdapter.getItem(position);
                marcaAdapter.setSelectedIndex(position);
                addMarcaBtn.setBackgroundColor(getResources().getColor(R.color.greenButton));
                addMarcaBtn.setText("SELECCIONAR MARCA : " + selectedMarca.getNombre());
            }
        });
        subRubroListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                subRubroSelected = subRubAdapter.getItem(position);
                subRubAdapter.setSelectedIndex(position);
                marcaAdapter.setMarcaList(marcaDAO.findBySubRubro(subRubroSelected));
                marcaAdapter.setSelectedIndex(-1);
            }
        });
        rubroSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                rubroSelected = rubroDAO.getHashMap().get(adapterRubros.getItem(position));
                subRubAdapter.setSubRubroList(subRubroDAO.getSubRubros(rubroSelected));
                subRubAdapter.notifyDataSetChanged();
                if(autoCall == false){

                    Log.d("Llamada:","Desde Spinner");
                    marcaAdapter.getMarcaList().clear();
                    marcaListView.clearChoices();
                    marcaAdapter.notifyDataSetChanged();
                    autoCall=false;
                }else
                    autoCall = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        addMarcaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedMarca == null) {

                    marcaAutoTextView.setError("Debe seleccionar una marca propuesta");
                } else {
                    long idRelevamiento = intent.getLongExtra("idRelevamiento", 0);
                    if (idRelevamiento != 0) {

                        Intent i = new Intent(OccupedSiteActivity.this, ModifyActivity.class);
                        i.putExtra("idRelevamiento", idRelevamiento);
                        i.putExtra("idMarca", selectedMarca.getId());
                        i.putExtra("idCalle", intent.getIntExtra("idCalle", 0))
                                .putExtra("orientacion", intent.getStringExtra("orientacion"))
                                .putExtra("numCalle", intent.getIntExtra("numCalle", 0));
                        startActivity(i);

                    } else
                        insertOccupedSite();
                }
            }
        });
        /*
        unkownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedMarca = marcaDAO.getHashMap().get("DESCONOCIDO");
                insertOccupedSite();
                Intent i = new Intent(OccupedSiteActivity.this, SurveyActivity.class)
                        .putExtra("idCalle", intent.getIntExtra("idCalle", 0))
                        .putExtra("orientacion", intent.getStringExtra("orientacion"))
                        .putExtra("numCalle", intent.getIntExtra("numCalle", 0));
                startActivity(i);
            }
        });*/
    }
    private void insertOccupedSite(){

        Calle calle = new Calle();
        calle.setId(intent.getIntExtra("idCalle",0));
        Lugar lugar = new Lugar();
        lugar.setId(Tipo.LOCAL_OCUPADO.getNumVal());
        Paridad paridad = intent.getIntExtra("paridad",0)==1?Paridad.PAR:Paridad.IMPAR;
        Relevamiento relevamiento = new Relevamiento();
        relevamiento.setNumeracion(intent.getIntExtra("numCalle",0));
        relevamiento.setParidad(paridad);
        relevamiento.setPosicion(intent.getIntExtra("posicion", 0));
        relevamiento.setSync(1);
        relevamiento.setRemoteId(Long.valueOf(0));
        relevamiento.setCalle(calle);
        relevamiento.setMarca(selectedMarca);
        relevamiento.setLugar(lugar);
        relevamiento.setOrientacion(intent.getStringExtra("orientacion"));
        long r = relevamientoDAO.insert(relevamiento);
        if(r>0) {
            Intent i = new Intent(OccupedSiteActivity.this, SurveyActivity.class)
                    .putExtra("idCalle", intent.getIntExtra("idCalle", 0))
                    .putExtra("orientacion", intent.getStringExtra("orientacion"))
                    .putExtra("numCalle", intent.getIntExtra("numCalle", 0));
            startActivity(i);
        }
    }

    @Override
    public void onBackPressed() {

        Intent i = new Intent(OccupedSiteActivity.this,SurveyActivity.class)
        .putExtra("idCalle",this.getIntent().getIntExtra("idCalle",0) )
        .putExtra("orientacion",this.getIntent().getStringExtra("orientacion"))
        .putExtra("numCalle",this.getIntent().getIntExtra("numCalle",0));
        startActivity(i);
    }

    private class InitAsync extends AsyncTask<Void,Void,Boolean>{

        private ArrayAdapter<String> adapterMarcas;
        private ProgressDialog pDialog;
        private Context mContext;

        public InitAsync(Context c){

            mContext = c;
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            marcas = marcaDAO.getAllArray();
            rubros = rubroDAO.getAllArray();
            return true;
        }

        @Override
        protected void onPreExecute() {

            marcaAdapter = new MarcaAdapter(context);
            subRubAdapter = new SubRubAdapter(context);

            pDialog = new ProgressDialog(mContext);
            pDialog.setTitle("Espere un momento");
            pDialog.setMessage("Cargando Información");
            pDialog.show();
        }
        @Override
        protected void onPostExecute(Boolean aBoolean) {

            adapterMarcas= new ArrayAdapter<String>(context,
                    android.R.layout.simple_dropdown_item_1line, marcas);
            marcaAutoTextView.setAdapter(adapterMarcas);
            adapterRubros = new ArrayAdapter<String>(mContext,
                    android.R.layout.simple_dropdown_item_1line, rubros);

            rubroSpinner.setAdapter(adapterRubros);
            subRubroListView.setAdapter(subRubAdapter);
            marcaListView.setAdapter(marcaAdapter);
            pDialog.dismiss();
        }
    }
}

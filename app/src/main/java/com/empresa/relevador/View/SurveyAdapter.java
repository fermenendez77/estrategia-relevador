package com.empresa.relevador.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.empresa.relevador.Model.Relevamiento;
import com.empresa.relevador.R;

import java.util.List;

/**
 * Created by fermenendez on 26/8/15.
 */

public class SurveyAdapter extends BaseAdapter {

    private Context mContext;
    private List<Relevamiento> relevamientos;
    public SurveyAdapter(Context context, List<Relevamiento> rels ){

        this.mContext = context;
        this.relevamientos = rels;
    }
    @Override
    public int getCount() {
        return relevamientos.size();
    }

    @Override
    public Relevamiento getItem(int position) {
        return relevamientos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.adapter_surveylist,null);
            holder.nameTextView = (TextView) view.findViewById(R.id.textInputName);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }
        if(relevamientos.get(position).getLugar().getTipo().equals("Local Ocupado")) {
            holder.nameTextView.setText(relevamientos.get(position).getPosicion() + " - " +
                    relevamientos.get(position).getMarca().getNombre());
        }
        else {

            holder.nameTextView.setText(relevamientos.get(position).getPosicion() + " - " +relevamientos.get(position).getLugar().getTipo());
        }
            return view;
    }
    private static class ViewHolder{

        TextView nameTextView;
    }

    public void setRelevamientos(List<Relevamiento> relevamientos) {
        this.relevamientos = relevamientos;
    }

    public List<Relevamiento> getRelevamientos() {
        return relevamientos;
    }
}
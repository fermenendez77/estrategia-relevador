package com.empresa.relevador.View;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.empresa.relevador.Controller.DAO.LugarDAO;
import com.empresa.relevador.Model.Tipo;

/**
 * Created by fermenendez on 30/8/15.
 */
public class SurveyTypeDialog extends DialogFragment {

    public static interface OnCompleteListener{

        public abstract void onComplete(Tipo tipo);
    }
    private LugarDAO lugarDAO;
    private OnCompleteListener mListener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        lugarDAO = LugarDAO.getInstance(getActivity().getBaseContext());
        final String[] types = {"Casa","Lote","Local Vacío","Local Ocupado","Edificio"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Tipo")
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.setItems(types, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                onSelectedItem(item);
            }
        });
        return builder.create();
    }

    public void onAttach(Activity activity){

        super.onAttach(activity);
        try{

            this.mListener = (OnCompleteListener) activity;
        }
        catch (final ClassCastException e){

        }
    }
    public void onSelectedItem(int item){

        switch (item){

            case 0:
                this.mListener.onComplete(Tipo.CASA);
                break;
            case 1:
                this.mListener.onComplete(Tipo.LOTE);
                break;
            case 2:
                this.mListener.onComplete(Tipo.LOCAL_VACIO);
                break;
            case 3:
                this.mListener.onComplete(Tipo.LOCAL_OCUPADO);
                break;
            case 4:
                final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setTitle("Tipo Edificio");
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                        getActivity(),
                        android.R.layout.select_dialog_item
                );
                arrayAdapter.add("Residencial");
                arrayAdapter.add("Residencial (Construcción)");
                arrayAdapter.add("Comercial");
                arrayAdapter.add("Comercial (Construcción)");
                dialog.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which){

                            case 0:
                                mListener.onComplete(Tipo.EDIFICIO_RESIDENCIAL);
                                break;
                            case 1:
                                mListener.onComplete(Tipo.EDIFICIO_RESIDENCIAL_CONSTRUCCION);
                                break;
                            case 2:
                                mListener.onComplete(Tipo.EDIFICIO_COMERCIAL);
                                break;
                            case 3:
                                mListener.onComplete(Tipo.EDIFICIO_COMERCIAL_CONSTRUCCION);
                                break;
                        }
                    }
                });
                dialog.show();
                break;
        }

    }
}


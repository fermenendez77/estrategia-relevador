package com.empresa.relevador.View;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.empresa.relevador.Controller.DAO.MarcaDAO;
import com.empresa.relevador.Controller.DAO.RelevamientoDAO;
import com.empresa.relevador.Controller.Service.CalleService;
import com.empresa.relevador.Controller.Service.CreationService;
import com.empresa.relevador.Controller.Service.MarcaService;
import com.empresa.relevador.Controller.Service.RelevamientoService;
import com.empresa.relevador.Controller.Service.SubRubroService;
import com.empresa.relevador.R;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends Activity {

    private Button newEntryBtn;
    private Button syncBtn;
    private Context mContext;
    private String fecha;
    private ProgressDialog dialog;
    private long sumPuts;
    private RelevamientoService relevamientoService;
    private MarcaService marcaService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        relevamientoService = new RelevamientoService(getApplicationContext());
        marcaService = new MarcaService(getApplicationContext());
        mContext = getApplicationContext();
        newEntryBtn = (Button) findViewById(R.id.newEntryBtn);
        syncBtn = (Button) findViewById(R.id.syncBtn);
        SharedPreferences prefs = getSharedPreferences("syncPreferences",Context.MODE_PRIVATE);
        fecha = prefs.getString("syncDate","first");
        if(fecha.equals("first")) {

            CreationService service = new CreationService(mContext);
            service.getCreationDate();
        }
        newEntryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this, NewEntryActivity.class);
                startActivity(i);
            }
        });

        syncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if (mWifi.isConnected()) {
                    dialog = ProgressDialog.show(MainActivity.this, "Por favor espere", "Sincronizando Información...",
                            true, false);
                    dialog.show();

                    final RelevamientoDAO relevamientoDAO = RelevamientoDAO.getInstance(mContext);
                    relevamientoDAO.revertDescendentes();
                    SharedPreferences prefs = getSharedPreferences("syncPreferences",Context.MODE_PRIVATE);
                    fecha = prefs.getString("syncDate","first");
                    Log.d("Date", fecha);
                    //Calle
                    CalleService calleService = new CalleService(getApplicationContext());
                    calleService.getNews(fecha);
                    //SubRubro
                    SubRubroService subRubroService = new SubRubroService(getApplicationContext());
                    subRubroService.getNews(fecha);
                    subRubroService.getUpdates(fecha);
                    //Marca

                    marcaService.getNews(fecha);
                    marcaService.getUpdates(fecha);
                    relevamientoService.putDeletes();
                    AsynSyncro sync = new AsynSyncro();
                    sync.execute();

                }
                else{

                    Toast.makeText(mContext,"Para realizar la sincronización debe estar conectado a una Red WIFI",Toast.LENGTH_LONG).show();
                }
            }
        });
        syncBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                RelevamientoDAO relevamientoDAO = RelevamientoDAO.getInstance(mContext);
                SharedPreferences prefs = getSharedPreferences("syncPreferences",Context.MODE_PRIVATE);
                String fecha = prefs.getString("syncDate","first");
                if(!fecha.equals("first")) {
                    String columns[] = fecha.split("%20");
                    Toast.makeText(getApplicationContext(), "Ultima Sincronización: " + columns[0] + " " + columns[1], Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Ultima Sincronización: " + " Espere a que se sincronice la hora", Toast.LENGTH_SHORT).show();

                }
                return true;
            }
        });
    }

    private void saveDate(){

        SharedPreferences prefs = getSharedPreferences("syncPreferences",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.SECOND,1);
        Log.d("Time",format.format(c.getTime()));
        editor.putString("syncDate",format.format(c.getTime()));
        editor.commit();
    }


    private class AsynSyncro extends AsyncTask<Void,String,Boolean>{

        @Override
        protected Boolean doInBackground(Void... params) {

            relevamientoService.getNews(fecha);
            publishProgress("Descargando Datos Actualizados");
            relevamientoService.getUpdates(fecha);
            publishProgress("Insertando datos Relevados en Servidor");
            relevamientoService.putNews();
            publishProgress("Actualizando Datos Relevados en Servidor");
            relevamientoService.putUpdates();
            return true;
        }

        @Override
        protected void onPreExecute() {

            dialog.setMessage("Descargando Datos Relevados");
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {

            dialog.dismiss();
            saveDate();
            marcaService.destroy();
        }

        @Override
        protected void onProgressUpdate(String... values) {

            dialog.setMessage(values[0]);
        }
    }
}

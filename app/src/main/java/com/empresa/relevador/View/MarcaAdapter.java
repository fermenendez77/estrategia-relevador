package com.empresa.relevador.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.empresa.relevador.Model.Marca;
import com.empresa.relevador.Model.Rubro;
import com.empresa.relevador.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fermenendez on 8/9/15.
 */
public class MarcaAdapter extends BaseAdapter {

    private List<Marca> MarcaList;
    private Context mContext;
    private int selectedIndex;
    public MarcaAdapter(Context context){

        this.MarcaList = new ArrayList<Marca>();
        this.mContext = context;
        this.selectedIndex = -1;
    }
    @Override
    public int getCount() {
        return MarcaList.size();
    }

    @Override
    public Marca getItem(int position) {
        return MarcaList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return MarcaList.get(position).getId() ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        View view = convertView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.adapter_marca,null);
            holder.nameTextView = (TextView) view.findViewById(R.id.textInputName);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder) view.getTag();
        }

        if(selectedIndex!= -1 && position == selectedIndex) {
            holder.nameTextView.setBackgroundColor(mContext.getResources().getColor(R.color.blueGoogle));
            holder.nameTextView.setTextColor(mContext.getResources().getColor(R.color.white));
        }
        else {
            holder.nameTextView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            holder.nameTextView.setTextColor(mContext.getResources().getColor(R.color.blueGoogle));
        }

        holder.nameTextView.setText(MarcaList.get(position).getNombre());
        return view;
    }
    private static class ViewHolder{

        TextView nameTextView;
    }

    public List<Marca> getMarcaList() {
        return MarcaList;
    }

    public void setMarcaList(List<Marca> marcaList) {
        MarcaList = marcaList;
    }

    public int getPosition(Marca marca){

        int pos = 0;
        for(Marca r: MarcaList){

            if(r.getId() == marca.getId()){

                return pos;
            }
            else{

                pos++;
            }
        }
        return -1;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
        notifyDataSetChanged();
    }
}

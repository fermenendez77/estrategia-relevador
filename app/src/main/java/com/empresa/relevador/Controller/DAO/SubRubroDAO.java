package com.empresa.relevador.Controller.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.empresa.relevador.Model.Rubro;
import com.empresa.relevador.Model.SubRubro;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fermenendez on 8/9/15.
 */
public class SubRubroDAO {

    static private SubRubroDAO instance = null;
    private Context mContext;
    private  SQLiteHelper dbHelper;
    private RubroDAO rubroDAO;
    private SubRubroDAO(Context context) {

        this.mContext = context;
        rubroDAO = RubroDAO.getInstance(context);
        this.dbHelper = new SQLiteHelper(context,"DB",null,DataBase.DATABASE_VERSION);

    }
    static public SubRubroDAO getInstance(Context context){
        if(instance==null){
            instance = new SubRubroDAO(context);
        }

        return instance;
    }

    public List<SubRubro> getSubRubros(Rubro rubro){

        List<SubRubro> subRubros = new ArrayList<SubRubro>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM SUBRUBRO WHERE ID_RUBRO = " + rubro.getId(), null);
            if (c.moveToFirst()) {
                do {

                    SubRubro subRubro = new SubRubro();
                    subRubro.setId(c.getInt(0));
                    subRubro.setNombre(c.getString(1));
                    subRubro.setSync(c.getInt(2));
                    subRubro.setRemoteId(c.getInt(3));
                    subRubro.setRubro(rubro);
                    subRubros.add(subRubro);
                } while (c.moveToNext());
            }
        }
        finally {

            if(c!=null)
                c.close();
        }
        db.close();
        return subRubros;
    }

    public SubRubro findById(long id) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM SUBRUBRO WHERE _ID = " + id, null);
            if (c.moveToFirst()) {

                SubRubro subRubro = new SubRubro();
                subRubro.setId(c.getInt(0));
                subRubro.setNombre(c.getString(1));
                subRubro.setSync(c.getInt(2));
                subRubro.setRemoteId(c.getInt(3));
                subRubro.setRubro(rubroDAO.findById(c.getInt(4)));
                db.close();
                return subRubro;
            } else
                return null;
        }
        finally {
            if(c!=null)
                c.close();
        }
    }

    public SubRubro findByRemoteId(long remoteId) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM SUBRUBRO WHERE REMOTE_ID = " + remoteId, null);
            if (c.moveToFirst()) {

                SubRubro subRubro = new SubRubro();
                subRubro.setId(c.getInt(0));
                subRubro.setNombre(c.getString(1));
                subRubro.setSync(c.getInt(2));
                subRubro.setRemoteId(c.getInt(3));
                subRubro.setRubro(rubroDAO.findById(c.getInt(4)));
                db.close();
                return subRubro;
            } else
                return null;
        }
        finally {
            if(c!=null)
                c.close();
        }
    }

    public long insert(SubRubro subRubro){

        long id;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("NOMBRE",subRubro.getNombre());
        values.put("SYNC",0);
        values.put("REMOTE_ID",0);
        values.put("ID_RUBRO",subRubro.getRubro().getId());
        id = db.insert("SUBRUBRO", null,values);
        db.close();
        return id;
    }

    public int update(SubRubro subRubro){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        String strFilter = "_ID = " + subRubro.getId();
        values.put("NOMBRE",subRubro.getNombre());
        values.put("SYNC",0);
        values.put("REMOTE_ID",subRubro.getId());
        values.put("ID_RUBRO",subRubro.getRubro().getId());
        int result = db.update("SUBRUBRO",values,strFilter,null);
        db.close();
        return result;
    }
}

package com.empresa.relevador.Controller.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.empresa.relevador.Model.Calle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by fermenendez on 30/8/15.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    private Context mContext;
    public SQLiteHelper(Context contexto, String nombre,
                                SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, nombre, factory, version);
        this.mContext = contexto;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DataBase.CALLE_TABLE);
        db.execSQL(DataBase.LUGAR_TABLE);
        db.execSQL(DataBase.RUBRO_TABLE);
        db.execSQL(DataBase.SUBRUBRO_TABLE);
        db.execSQL(DataBase.MARCA_TABLE);
        db.execSQL(DataBase.RELEVAMIENTO_TABLE);
        db.execSQL(DataBase.LUGAR);
        insertCalles(db, "calles.csv");
        insertMarca(db,"marcas.csv");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


     }

    private void insertCalles(SQLiteDatabase db,String filename){

        AssetManager manager = mContext.getAssets();
        InputStream inputStream = null;
        try {
            inputStream = manager.open(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        try {
            while((line = buffer.readLine()) != null){

                String[] columns = line.split(";");
                ContentValues cv = new ContentValues();
                cv.put("NOMBRE",columns[0]);
                cv.put("MAXNUM",Integer.valueOf(columns[1]).intValue());
                db.insert("CALLE",null,cv);
            }
        } catch (IOException e) {
            Log.i("Resultado  Calles:","Incorrecto");
            e.printStackTrace();
        }
    }

    private void insertMarca(SQLiteDatabase db,String filename){


        AssetManager manager = mContext.getAssets();
        InputStream inputStream = null;
        try {
            inputStream = manager.open(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        long idRubro = 0;
        long idSubrubro = 0;
        try {
            while((line = buffer.readLine()) != null){

                String[] columns = line.split(";");
                if(!columns[0].equals("")){

                    ContentValues cvRubro = new ContentValues();
                    cvRubro.put("NOMBRE",columns[0]);
                    idRubro = db.insert("RUBRO",null,cvRubro);
                }
                if(!columns[1].equals("")){

                    ContentValues cvSubrubro = new ContentValues();
                    cvSubrubro.put("NOMBRE",columns[1]);
                    cvSubrubro.put("SYNC",1);
                    cvSubrubro.put("REMOTE_ID",1);
                    cvSubrubro.put("ID_RUBRO",idRubro);
                    idSubrubro = db.insert("SUBRUBRO",null,cvSubrubro);
                }
                ContentValues cvMarca = new ContentValues();
                cvMarca.put("NOMBRE",columns[2]);
                cvMarca.put("SYNC",1);
                cvMarca.put("REMOTE_ID",1);
                cvMarca.put("ID_SUBRUBRO",idSubrubro);
                cvMarca.put("ID_RUBRO",idRubro);
                db.insert("MARCA",null,cvMarca);
            }
        } catch (IOException e) {
            Log.i("Resultado  Marcas:","Incorrecto");
            e.printStackTrace();
        }
    }
    private void updateCalles(SQLiteDatabase db){

        ContentValues cv = new ContentValues();
        cv.put("NOMBRE", "HIPOLITO YRIGOYEN");
        cv.put("MAXNUM", 10000);
        db.insert("CALLE", null, cv);
    }
    private void updateMarcas(SQLiteDatabase db,String filename){

        AssetManager manager = mContext.getAssets();
        InputStream inputStream = null;
        try {
            inputStream = manager.open(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedReader buffer = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        long idRubro = 0;
        long idSubrubro = 0;
        try {
            while((line = buffer.readLine()) != null) {

                String[] columns = line.split(";",4);

                if (!columns[0].equals("")) {

                    Cursor c = null;
                    try{
                        c = db.rawQuery("SELECT _ID FROM RUBRO WHERE NOMBRE = '"+columns[0]+"'",null);
                        if(c.moveToFirst())
                            idRubro = c.getLong(0);
                    }
                    finally {
                        if(c!=null)
                            c.close();
                    }
                }
                if (!columns[1].equals("")) {

                    Cursor c = null;
                    try{
                        c = db.rawQuery("SELECT _ID FROM SUBRUBRO WHERE NOMBRE = '"+columns[1]+"'",null);
                        if(c.moveToFirst())
                            idSubrubro = c.getLong(0);
                        else{

                            ContentValues cvSubrubro = new ContentValues();
                            cvSubrubro.put("NOMBRE", columns[2]);
                            cvSubrubro.put("SYNC", 1);
                            cvSubrubro.put("REMOTE_ID", 1);
                            cvSubrubro.put("ID_RUBRO", idRubro);
                            idSubrubro = db.insert("SUBRUBRO", null, cvSubrubro);
                        }
                    }
                    finally {
                        if(c!=null)
                            c.close();
                    }
                }
                if (columns[3].equals("1")) {
                    ContentValues cvMarca = new ContentValues();
                    cvMarca.put("NOMBRE", columns[2]);
                    cvMarca.put("SYNC", 1);
                    cvMarca.put("REMOTE_ID", 1);
                    cvMarca.put("ID_SUBRUBRO", idSubrubro);
                    cvMarca.put("ID_RUBRO", idRubro);
                    db.insert("MARCA", null, cvMarca);
                }
            }
        } catch (IOException e) {
            Log.i("Resultado  Marcas:","Incorrecto");
            e.printStackTrace();
        }
    }
}

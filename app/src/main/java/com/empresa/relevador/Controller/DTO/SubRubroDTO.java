package com.empresa.relevador.Controller.DTO;

import com.empresa.relevador.Model.SubRubro;

import java.util.List;

/**
 * Created by fermenendez on 22/10/15.
 */
public class SubRubroDTO {

    private List<SubRubro> objects;

    public List<SubRubro> getObjects() {
        return objects;
    }

    public void setObjects(List<SubRubro> objects) {
        this.objects = objects;
    }
}

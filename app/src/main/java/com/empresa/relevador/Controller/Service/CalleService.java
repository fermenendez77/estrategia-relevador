package com.empresa.relevador.Controller.Service;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.empresa.relevador.Controller.DAO.CalleDAO;
import com.empresa.relevador.Controller.DTO.CalleDTO;
import com.empresa.relevador.Model.Calle;

/**
 * Created by fermenendez on 22/10/15.
 */
public class CalleService extends AbstractService {

    private Context mContext;
    private CalleDAO calleDAO;
    private final String CALLE = "calle/";

    public CalleService(Context context){

        this.mContext = context;
        calleDAO = CalleDAO.getInstance(context);
    }

    public void getNews(String fechaCreacion){

        String PARAMS = "getNews?date="+fechaCreacion;
        VolleySingleton.getInstance(mContext).addToRequestQueue(
                new GsonRequest<CalleDTO>(
                        BASE_URL+CALLE+PARAMS,
                        CalleDTO.class,
                        null,
                        new Response.Listener<CalleDTO>() {
                            @Override
                            public void onResponse(CalleDTO response) {
                                int count = 0;
                                for (Calle calle : response.getObjects()) {

                                    calleDAO.insert(calle);
                                    count++;
                                }
                                Log.d("Calles ingresadas:",String.valueOf(count));
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error", "Error Respuesta en JSON: " + error.getMessage());
                                return;
                            }
                        }
                ){

                    @Override
                    public Priority getPriority() {
                        return Priority.IMMEDIATE;
                    }
                }
        );
    }
}

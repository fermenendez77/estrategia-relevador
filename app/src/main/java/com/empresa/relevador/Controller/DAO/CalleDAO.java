package com.empresa.relevador.Controller.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.empresa.relevador.Model.Calle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fermenendez on 3/9/15.
 */
public class CalleDAO {

    //Singleton
    static private CalleDAO instance = null;
    private Context mContext;
    private  SQLiteHelper dbHelper;
    private CalleDAO(Context context) {

        this.mContext = context;
        this.dbHelper = new SQLiteHelper(context,"DB",null,DataBase.DATABASE_VERSION);

    }
    static public CalleDAO getInstance(Context context){
        if(instance==null){
            instance = new CalleDAO(context);
        }

        return instance;
    }

    public List<Calle> getAll(){

        List<Calle> calleList = new ArrayList<Calle>();
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        Cursor c = null;
        c = db.rawQuery("SELECT * FROM CALLE ORDER BY _ID",null);
        try {
            if (c.moveToFirst()) {
                do {

                    Calle calle = new Calle();
                    calle.setId(c.getInt(0));
                    calle.setNombre(c.getString(1));
                    calle.setMaxNum(c.getInt(2));
                    calleList.add(calle);
                } while (c.moveToNext());
            }
        }
        finally {
            if(c!=null)
                db.close();
        }
        return calleList;
    }

    public Calle findById(Integer id) {

        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM CALLE WHERE _ID = " + id, null);
            if (c.moveToFirst()) {

                Calle calle = new Calle();
                calle.setId(c.getInt(0));
                calle.setNombre(c.getString(1));
                calle.setMaxNum(c.getInt(2));
                c.close();
                db.close();
                return calle;
            } else
                return null;
        }finally {
            if(c!=null)
                c.close();
        }
    }

    public long insert(Calle calle){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("NOMBRE",calle.getNombre());
        values.put("MAXNUM",calle.getMaxNum());
        return db.insert("CALLE",null,values);
    }
}
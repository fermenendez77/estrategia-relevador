package com.empresa.relevador.Controller.DTO;

import com.empresa.relevador.Model.Relevamiento;

import java.util.List;

/**
 * Created by fermenendez on 27/10/15.
 */
public class RelevamientoDTO {

    private List<Relevamiento> objects;

    public List<Relevamiento> getObjects() {
        return objects;
    }

    public void setObjects(List<Relevamiento> objects) {
        this.objects = objects;
    }
}

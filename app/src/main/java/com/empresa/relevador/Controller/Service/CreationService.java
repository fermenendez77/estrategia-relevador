package com.empresa.relevador.Controller.Service;

import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.empresa.relevador.Controller.DTO.CalleDTO;
import com.empresa.relevador.Controller.DTO.Creation;
import com.empresa.relevador.Model.Calle;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by fermenendez on 26/10/15.
 */
public class CreationService extends AbstractService {

    private Context mContext;
    private final String CREATION = "creation/";
    private Creation creation;

    public CreationService(Context context){

        this.mContext = context;
    }

    public void getCreationDate(){

        String PARAMS = "get";
        String string = BASE_URL+CREATION+PARAMS;
        VolleySingleton.getInstance(mContext).addToRequestQueue(
                new GsonRequest<Creation>(
                        BASE_URL+CREATION+PARAMS,
                        Creation.class,
                        null,
                        new Response.Listener<Creation>() {
                            @Override
                            public void onResponse(Creation response) {

                                Log.d("JSON",response.getCreationDate());
                                SharedPreferences prefs = mContext.getSharedPreferences("syncPreferences",Context.MODE_PRIVATE);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss");
                                Calendar c = Calendar.getInstance();
                                try {
                                    c.setTime(format.parse(response.getCreationDate()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                c.add(Calendar.SECOND,2);
                                SharedPreferences.Editor editor = prefs.edit();
                                String s = format.format(c.getTime());
                                editor.putString("syncDate",s);
                                editor.commit();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error", "Error Respuesta en JSON: " + error.getMessage());

                            }
                        }
                )
        );

    }

    public Creation getCreation() {
        return creation;
    }

    public void setCreation(Creation creation) {
        this.creation = creation;
    }
}

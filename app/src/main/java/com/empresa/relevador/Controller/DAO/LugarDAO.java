package com.empresa.relevador.Controller.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.empresa.relevador.Model.Calle;
import com.empresa.relevador.Model.Lugar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by fermenendez on 7/9/15.
 */
public class LugarDAO {

    //Singleton
    static private LugarDAO instance = null;
    private Context mContext;
    private SQLiteHelper dbHelper;
    private HashMap<String,Lugar> hashMap;
    private LugarDAO(Context context) {

        this.mContext = context;
        this.dbHelper = new SQLiteHelper(context,"DB",null,DataBase.DATABASE_VERSION);
        hashMap = new HashMap<String,Lugar>();

    }
    static public LugarDAO getInstance(Context context){
        if(instance==null){
            instance = new LugarDAO(context);
        }

        return instance;
    }

    public Lugar findById (Integer id){

        List<Calle> calleList = new ArrayList<Calle>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM LUGAR WHERE _ID = " + id, null);
            if (c.moveToFirst()) {

                Lugar lugar = new Lugar();
                lugar.setId(c.getInt(0));
                lugar.setTipo(c.getString(1));
                db.close();
                return lugar;
            } else {

                return null;
            }
        }
        finally {
            if(c!=null)
                c.close();
        }
    }

    public List<Lugar> getAll(){

        List<Lugar> lugarList = new ArrayList<Lugar>();
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM LUGAR", null);
            if (c.moveToFirst()) {
                do {

                    Lugar lugar = new Lugar();
                    lugar.setId(c.getInt(0));
                    lugar.setTipo(c.getString(1));
                    lugarList.add(lugar);
                    hashMap.put(lugar.getTipo().toString(), lugar);
                } while (c.moveToNext());
            }
        }
        finally {

            if(c!=null)
                c.close();
        }
        db.close();
        return lugarList;
    }

    public String[] getAllArray(){

        List<String> names = new ArrayList<String>();
        for (Lugar lugar : getAll()){

            names.add(lugar.getTipo());
        }
        String[] arrayNames = names.toArray(new String[names.size()]);
        return arrayNames;
    }

    public HashMap<String, Lugar> getHashMap() {
        return hashMap;
    }
}

package com.empresa.relevador.Controller.DTO;

import com.empresa.relevador.Model.Marca;

import java.util.List;

/**
 * Created by fermenendez on 22/10/15.
 */
public class MarcaDTO {

    private List<Marca> objects;

    public List<Marca> getObjects() {
        return objects;
    }

    public void setObjects(List<Marca> objects) {
        this.objects = objects;
    }
}

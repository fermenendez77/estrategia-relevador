package com.empresa.relevador.Controller.DAO;

/**
 * Created by fermenendez on 3/9/15.
 */
public class DataBase {

    public static final int DATABASE_VERSION = 3;
    public static final String CALLE_TABLE = "CREATE TABLE CALLE (\n" +
            "\t_ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\tNOMBRE TEXT NOT NULL,\n" +
            "\tMAXNUM INTEGER NOT NULL);\n";
    public static final String RUBRO_TABLE = "CREATE TABLE RUBRO (\n" +
            "\n" +
            "\t_ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\tNOMBRE TEXT NOT NULL);\n";

    public static final String SUBRUBRO_TABLE = "CREATE TABLE SUBRUBRO (\n" +
            "\n" +
            "\t_ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\tNOMBRE TEXT NOT NULL,\n" +
            "\tSYNC INTEGER NOT NULL,\n" +
            "\tREMOTE_ID INTEGER,\n" +
            "\tID_RUBRO INTEGER NOT NULL,\n" +
            "\tFOREIGN KEY (ID_RUBRO) REFERENCES RUBRO(_ID));\n";

    public static final String MARCA_TABLE ="CREATE TABLE MARCA (\n" +
            "\n" +
            "\t_ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\tNOMBRE TEXT NOT NULL,\n" +
            "\tSYNC INTEGER NOT NULL,\n" +
            "\tREMOTE_ID INTEGER,\n" +
            "\tID_SUBRUBRO INTEGER,\n" +
            "\tID_RUBRO INTEGER NOT NULL,\n" +
            "\tFOREIGN KEY (ID_RUBRO) REFERENCES RUBRO(_ID),\n" +
            "\tFOREIGN KEY (ID_SUBRUBRO) REFERENCES SUBRUBRO(_ID));";

    public static final String LUGAR_TABLE = "CREATE TABLE LUGAR (\n" +
            "\n" +
            "\t_ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\tTIPO TEXT NOT NULL);\n" +
            "\n";
    public static final String RELEVAMIENTO_TABLE = "CREATE TABLE RELEVAMIENTO (\n" +
            "\n" +
            "\t_ID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
            "\tNUMERACION INTEGER NOT NULL,\n" +
            "\tPARIDAD INTEGER NOT NULL,\n" +
            "\tPOSICION INTEGER NOT NULL,\n" +
            "\tSYNC INTEGER NOT NULL,\n" +
            "\tREMOTE_ID INTEGER,\n" +
            "\tFECHA TEXT NOT NULL,\n" +
            "\tID_CALLE INTEGER NOT NULL,\n" +
            "\tID_LUGAR INTERGER NOT NULL,\n" +
            "\tID_MARCA INTEGER,\n" +
            "\tACTIVO INTEGER NOT NULL,\n" +
            "\tORIENTACION TEXT NOT NULL,\n" +
            "\tFOREIGN KEY (ID_MARCA) REFERENCES MARCA(_ID),\n" +
            "\tFOREIGN KEY (ID_CALLE) REFERENCES CALLE(_ID),\n" +
            "\tFOREIGN KEY (ID_LUGAR) REFERENCES LUGAR(_ID)\t\n" +
            "); ";

    public static final String CALLES = "INSERT INTO CALLE (NOMBRE,MAXNUM) VALUES" +
            "(\"AV. COLON\",2000)," +
            "(\"BV. ILLIA\",700)," +
            "(\"BV. CHACABUCO\",1100)," +
            "(\"RONDEAU\",500)," +
            "(\"BV. SAN JUAN\",1500)," +
            "(\"SAN LORENZO\",500)," +
            "(\"AV. PUEYRREDON\",800)," +
            "(\"IRIGOYEN\",400)," +
            "(\"BUENOS AIRES\",1000)," +
            "(\"ESTRADA\",500);";

    public static final String LUGAR = "INSERT INTO LUGAR (TIPO) VALUES (\"Casa\"),(\"Lote\"),(\"Local Vacío\"),(\"Local Ocupado\"),(\"Edificio Residencial\"),(\"Edificio Residencial (Construcción)\"),(\"Edificio Comercial\"),(\"Edificio Comercial (Construcción)\");\n";
}

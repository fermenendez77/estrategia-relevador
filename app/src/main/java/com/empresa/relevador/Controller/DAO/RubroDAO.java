package com.empresa.relevador.Controller.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.empresa.relevador.Model.Rubro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by fermenendez on 8/9/15.
 */
public class RubroDAO {

    static private RubroDAO instance = null;
    private Context mContext;
    private  SQLiteHelper dbHelper;
    private HashMap<String,Rubro> hashMap;
    private RubroDAO(Context context) {

        this.hashMap = new HashMap<String,Rubro>();
        this.mContext = context;
        this.dbHelper = new SQLiteHelper(context,"DB",null,DataBase.DATABASE_VERSION);

    }
    static public RubroDAO getInstance(Context context){
        if(instance==null){
            instance = new RubroDAO(context);
        }

        return instance;
    }

    public List<Rubro> findAll(){

        List<Rubro> rubroList = new ArrayList<Rubro>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
         c = db.rawQuery("SELECT * FROM RUBRO ", null);
            if (c.moveToFirst()) {
                do {

                    Rubro rubro = new Rubro();
                    rubro.setId(c.getInt(0));
                    rubro.setNombre(c.getString(1));
                    hashMap.put(rubro.getNombre(), rubro);
                    rubroList.add(rubro);
                } while (c.moveToNext());
            }
        }
        finally {

            if(c!=null)
                c.close();
        }
        db.close();
        return rubroList;
    }

    public String[] getAllArray() {

        List<String> listStrings = new ArrayList<String>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        if (hashMap.isEmpty()) {
            try {
                c = db.rawQuery("SELECT * FROM RUBRO ", null);
                if (c.moveToFirst()) {
                    do {

                        Rubro rubro = new Rubro();
                        rubro.setId(c.getInt(0));
                        rubro.setNombre(c.getString(1));
                        hashMap.put(rubro.getNombre(), rubro);
                        listStrings.add(rubro.getNombre());
                    } while (c.moveToNext());
                }
            } finally {
                if (c != null)
                    c.close();
            }
            db.close();
            String[] array = listStrings.toArray(new String[listStrings.size()]);
            return array;
        }else {
            for (String key : hashMap.keySet()) {
                listStrings.add(key);
            }
            String[] array = listStrings.toArray(new String[listStrings.size()]);
            return array;

        }
    }

    public HashMap<String, Rubro> getHashMap() {
        return hashMap;
    }


    public Rubro findById(long id){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM RUBRO WHERE _ID = " + id, null);
            if (c.moveToFirst()) {

                Rubro rubro = new Rubro();
                rubro.setId(c.getInt(0));
                rubro.setNombre(c.getString(1));
                db.close();
                return rubro;
            } else
                return null;
        }
        finally {

            if(c!=null)
                c.close();
        }

     }

}

package com.empresa.relevador.Controller.Service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.empresa.relevador.Controller.DAO.MarcaDAO;
import com.empresa.relevador.Controller.DTO.MarcaDTO;
import com.empresa.relevador.Controller.DTO.SubRubroDTO;
import com.empresa.relevador.Model.Marca;
import com.empresa.relevador.Model.SubRubro;

/**
 * Created by fermenendez on 22/10/15.
 */
public class MarcaService extends AbstractService {

    private Context mContext;
    private MarcaDAO marcaDAO;
    private final String MARCA = "marca/";

    public MarcaService(Context context){

        mContext = context;
        marcaDAO = MarcaDAO.getInstance(context);
    }

    public void getNews(String fechaCreacion){

        String PARAMS = "getNews?date="+fechaCreacion;
        VolleySingleton.getInstance(mContext).addToRequestQueue(
                new GsonRequest<MarcaDTO>(
                        BASE_URL+MARCA+PARAMS,
                        MarcaDTO.class,
                        null,
                        new Response.Listener<MarcaDTO>() {
                            @Override
                            public void onResponse(MarcaDTO response) {
                                int count = 0;
                                for (Marca marca : response.getObjects()) {

                                    marcaDAO.insert(marca);
                                    count++;
                                }
                                Log.d("MarcasService","Se insertaron "+ count + "Marcas Nuevas");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error", "Error Respuesta en JSON: " + error.getMessage());
                                return;
                            }
                        }
                ){

                    @Override
                    public Priority getPriority() {
                        return Priority.HIGH;
                    }
                }
        );
    }
    public void getUpdates(String fechaCreacion){

        String PARAMS = "getUpdates?date="+fechaCreacion;
        VolleySingleton.getInstance(mContext).addToRequestQueue(
                new GsonRequest<MarcaDTO>(
                        BASE_URL+MARCA+PARAMS,
                        MarcaDTO.class,
                        null,
                        new Response.Listener<MarcaDTO>() {
                            @Override
                            public void onResponse(MarcaDTO response) {

                                int count = 0;
                                for (Marca marca : response.getObjects()) {

                                    marcaDAO.update(marca);
                                    count++;
                                }
                                Log.d("MarcasService","Se actualizaron "+ count + "Marcas Nuevas");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error", "Error Respuesta en JSON: " + error.getMessage());
                                return;
                            }
                        }
                ){

                    @Override
                    public Priority getPriority() {
                        return Priority.NORMAL;
                    }
                }
        );
    }

    public void destroy(){

        marcaDAO.destroy();
    }

}

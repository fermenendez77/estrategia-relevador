package com.empresa.relevador.Controller.Service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.empresa.relevador.Controller.DAO.MarcaDAO;
import com.empresa.relevador.Controller.DAO.RelevamientoDAO;
import com.empresa.relevador.Controller.DTO.MarcaDTO;
import com.empresa.relevador.Controller.DTO.RelevamientoDTO;
import com.empresa.relevador.Controller.DTO.ResponseRelevamientoDTO;
import com.empresa.relevador.Model.Marca;
import com.empresa.relevador.Model.Relevamiento;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by fermenendez on 27/10/15.
 */
public class RelevamientoService extends AbstractService {

    private Context mContext;
    private RelevamientoDAO relevamientoDAO;
    private final String RELEVAMIENTO = "relevamiento/";
    private long puts;
    private long updates;

    public RelevamientoService(Context context){

        this.mContext = context;
        this.relevamientoDAO = RelevamientoDAO.getInstance(mContext);
        this.puts = 0;
        this.updates = 0;
    }

    public void getNews(final String fechaCreacion) {


        String PARAMS = "getNews?date=" + fechaCreacion;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet del = new HttpGet(BASE_URL + RELEVAMIENTO + PARAMS);
        del.setHeader("content-type", "application/json");
        try {
            HttpResponse resp = httpClient.execute(del);
            //String respStr = EntityUtils.toString(resp.getEntity(),"UTF-8");
            String respStr = new String(EntityUtils.toString(resp.getEntity()).getBytes("ISO-8859-1"), "UTF-8");

            Gson gson = new Gson();
            RelevamientoDTO response = gson.fromJson(respStr, RelevamientoDTO.class);
            long count = 0;
            for (Relevamiento relevamiento : response.getObjects()) {

                relevamientoDAO.insertNews(relevamiento);
                count++;
            }
            Log.d("RelevamientoService", "Se insertaron " + count + "Relevamientos");

        } catch (Exception ex) {
            Log.e("ServicioRest", "Error!", ex);
        }
    }

    public void getUpdates(String fechaModificacion){

        String PARAMS = "getUpdates?date="+fechaModificacion;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet del = new HttpGet(BASE_URL + RELEVAMIENTO + PARAMS);
        del.setHeader("content-type", "application/json");
        try {
            HttpResponse resp = httpClient.execute(del);
            String respStr = new String(EntityUtils.toString(resp.getEntity()).getBytes("ISO-8859-1"), "UTF-8");
            Gson gson = new Gson();
            RelevamientoDTO response = gson.fromJson(respStr, RelevamientoDTO.class);
            long count = 0;
            for (Relevamiento relevamiento : response.getObjects()) {

                relevamientoDAO.remoteUpdate(relevamiento.getId(), relevamiento);
                count++;
            }
            Log.d("RelevamientoService", "Se insertaron " + count + "Relevamientos");

        } catch (Exception ex) {
            Log.e("ServicioRest", "Error!", ex);
        }
    }

    public void putDeletes(){

        List<Relevamiento> relevamientos = relevamientoDAO.findDeletes();
        for (final Relevamiento relevamiento : relevamientos) {
            String PARAMS = "delete?id=" + relevamiento.getRemoteId();
            VolleySingleton.getInstance(mContext).addToRequestQueue(
                    new GsonRequest<RelevamientoDTO>(
                            BASE_URL + RELEVAMIENTO + PARAMS,
                            RelevamientoDTO.class,
                            null,
                            new Response.Listener<RelevamientoDTO>() {
                                @Override
                                public void onResponse(RelevamientoDTO response) {

                                    relevamientoDAO.delete(relevamiento);
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("Error", "Error Respuesta en JSON: " + error.getMessage());
                                    return;
                                }
                            }
                    ){

                        @Override
                        public Priority getPriority() {
                            return Priority.HIGH;
                        }
                    }
            );
        }
    }

    public void putNews() {

        String PARAMS = "put";
        List<Relevamiento> relevamientos = relevamientoDAO.findNewsRelevamientos();
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost post = new HttpPost(BASE_URL + RELEVAMIENTO + PARAMS);
        post.setHeader("content-type", "application/json");

        try {

            for(Relevamiento relevamiento : relevamientos) {

                Gson gsonSend = new Gson();
                JSONObject dato = new JSONObject(gsonSend.toJson(relevamiento));
                StringEntity entity = new StringEntity(dato.toString(),"UTF-8");
                post.setEntity(entity);
                HttpResponse resp = httpClient.execute(post);
                String respStr = EntityUtils.toString(resp.getEntity());
                Gson g = new Gson();
                ResponseRelevamientoDTO dto = g.fromJson(respStr,ResponseRelevamientoDTO.class);
                relevamiento.setRemoteId(dto.getRemoteID());
                relevamiento.setSync(0);
                relevamientoDAO.updateNews(relevamiento.getId(),relevamiento);
            }
        }
        catch(Exception ex)
        {
            Log.e("ServicioRest","Error!", ex);
            return;
        }

    }

    public void putUpdates()  {

        String PARAMS = "update";
        List<Relevamiento> relevamientos = relevamientoDAO.findUpdatesRelevamientos();
        HttpClient httpClient = new DefaultHttpClient();
        HttpPut put = new HttpPut(BASE_URL + RELEVAMIENTO + PARAMS);
        put.setHeader("content-type", "application/json");

        try {
            //Construimos el objeto cliente en formato JSON
            for(Relevamiento relevamiento : relevamientos) {

                Gson gsonSend = new Gson();
                JSONObject dato = new JSONObject(gsonSend.toJson(relevamiento));


                StringEntity entity = new StringEntity(dato.toString(),"UTF-8");
                put.setEntity(entity);

                HttpResponse resp = httpClient.execute(put);
                String respStr = EntityUtils.toString(resp.getEntity());
                Gson g = new Gson();
                ResponseRelevamientoDTO dto = g.fromJson(respStr,ResponseRelevamientoDTO.class);
                relevamiento.setRemoteId(dto.getRemoteID());
                relevamiento.setSync(0);
                relevamientoDAO.updateNews(relevamiento.getId(),relevamiento);
            }
        }
        catch(Exception ex)
        {
            Log.e("ServicioRest","Error!", ex);
            return;
        }

    }

    public void putUpdates2() throws JSONException {

        Gson gson = new Gson();
        String PARAMS = "update";
        List<Relevamiento> relevamientos = relevamientoDAO.findUpdatesRelevamientos();
        updates = relevamientos.size();
        for(final Relevamiento relevamiento : relevamientos) {

            JsonObjectRequest jsArrayRequest = new JsonObjectRequest(
                    Request.Method.PUT,
                    BASE_URL + RELEVAMIENTO + PARAMS,
                    new JSONObject(gson.toJson(relevamiento)),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            Gson g = new Gson();
                            ResponseRelevamientoDTO dto = g.fromJson(response.toString(),ResponseRelevamientoDTO.class);
                            relevamiento.setRemoteId(dto.getRemoteID());
                            relevamiento.setSync(0);
                            relevamientoDAO.updateNews(relevamiento.getId(),relevamiento);
                            Log.d("ResponseDTO",response.toString());
                        }
                    },
                    new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            // Manejo de errores

                        }
                    }){
                @Override
                public Priority getPriority() {
                    return Priority.LOW;
                }
            };
            VolleySingleton.getInstance(mContext).addToRequestQueue(jsArrayRequest);
        }
    }

    public long getPuts() {
        return puts;
    }

    public long getUpdates() {
        return updates;
    }
}

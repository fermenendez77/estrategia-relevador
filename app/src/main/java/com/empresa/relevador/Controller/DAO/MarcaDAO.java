package com.empresa.relevador.Controller.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.empresa.relevador.Model.Lugar;
import com.empresa.relevador.Model.Marca;
import com.empresa.relevador.Model.Rubro;
import com.empresa.relevador.Model.SubRubro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by fermenendez on 8/9/15.
 */
public class MarcaDAO {

    static private MarcaDAO instance = null;
    private Context mContext;
    private  SQLiteHelper dbHelper;
    private SubRubroDAO subRubroDAO;
    private RubroDAO rubroDAO;
    private HashMap<String,Marca> hashMap;
    private MarcaDAO(Context context) {

        this.subRubroDAO = SubRubroDAO.getInstance(context);
        this.hashMap = new HashMap<String,Marca>();
        this.mContext = context;
        this.rubroDAO = RubroDAO.getInstance(context);
        this.dbHelper = new SQLiteHelper(context,"DB",null,DataBase.DATABASE_VERSION);
    }
    static public MarcaDAO getInstance(Context context){
        if(instance==null){
            instance = new MarcaDAO(context);
        }

        return instance;
    }

    public String[] getAllArray(){

        List<String> marcasStrings = new ArrayList<String>();

        if(hashMap.isEmpty()) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            Cursor c = null;
            try {
                c = db.rawQuery("SELECT * FROM MARCA ", null);
                if (c.moveToFirst()) {
                    do {

                        Marca marca = new Marca();
                        marca.setId(c.getLong(0));
                        marca.setNombre(c.getString(1));
                        marca.setSync(c.getInt(2));
                        marca.setRemoteId(c.getInt(3));
                        marca.setSubRubro(subRubroDAO.findById(c.getLong(4)));
                        marca.setRubro(rubroDAO.findById(c.getLong(5)));
                        hashMap.put(marca.getNombre(), marca);
                        marcasStrings.add(marca.getNombre());
                    } while (c.moveToNext());
                }
            }finally {
                if(c!=null)
                    c.close();
            }
            db.close();
            String[] array = marcasStrings.toArray(new String[marcasStrings.size()]);
            return array;
        }
        else{

            for(String key:hashMap.keySet()){

                marcasStrings.add(key);
            }
            String[] array = marcasStrings.toArray(new String[marcasStrings.size()]);
            return array;
        }
    }

    public List<Marca> findBySubRubro(SubRubro subRubro){

        List<Marca> marcas = new ArrayList<Marca>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM MARCA WHERE ID_SUBRUBRO = " + subRubro.getId(), null);
            if (c.moveToFirst()) {
                do {

                    Marca marca = new Marca();
                    marca.setId(c.getLong(0));
                    marca.setNombre(c.getString(1));
                    marca.setSync(c.getInt(2));
                    marca.setRemoteId(c.getInt(3));
                    marca.setSubRubro(subRubro);
                    marca.setRubro(subRubro.getRubro());
                    marcas.add(marca);
                } while (c.moveToNext());
            }
        }
        finally {
            if(c!=null)
                c.close();
        }
        db.close();
        return marcas;
    }

    public List<Marca> findByRubro(Rubro rubro){

        List<Marca> marcas = new ArrayList<Marca>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM MARCA WHERE ID_RUBRO = " + rubro.getId(), null);
            if (c.moveToFirst()) {
                do {

                    Marca marca = new Marca();
                    marca.setId(c.getLong(0));
                    marca.setNombre(c.getString(1));
                    marca.setSync(c.getInt(2));
                    marca.setRemoteId(c.getInt(3));
                    if (!c.isNull(4)) {

                        SubRubro subRubro = subRubroDAO.findById(c.getLong(4));
                        marca.setSubRubro(subRubro);
                    }
                    marca.setRubro(rubro);
                    marcas.add(marca);
                } while (c.moveToNext());
            }
        }
        finally {
            if(c!=null)
                c.close();
        }
        db.close();
        return marcas;
    }
    public Marca findbyId(long id) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM MARCA WHERE _ID = " + id, null);
            if (c.moveToFirst()) {

                Marca marca = new Marca();
                marca.setId(c.getLong(0));
                marca.setNombre(c.getString(1));
                marca.setSync(c.getInt(2));
                marca.setRemoteId(c.getInt(3));
                SubRubro subRubro = subRubroDAO.findById(c.getInt(4));
                marca.setSubRubro(subRubro);
                Rubro rubro = rubroDAO.findById(c.getInt(5));
                marca.setRubro(rubro);
                c.close();
                db.close();
                return marca;
            } else
                return null;
        }
        finally {
            if(c!=null)
                c.close();
        }
    }

    public long insert(Marca marca){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("NOMBRE",marca.getNombre());
        values.put("SYNC",0);
        values.put("REMOTE_ID",0);
        if(marca.getSubRubro() != null)
            values.put("ID_SUBRUBRO",marca.getSubRubro().getId());
        values.put("ID_RUBRO",marca.getRubro().getId());
        return db.insert("MARCA", null,values);
    }

    public int update(Marca marca){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        String strFilter = "_ID = " + marca.getId();
        values.put("NOMBRE",marca.getNombre());
        values.put("SYNC",0);
        values.put("REMOTE_ID",marca.getId());
        values.put("ID_RUBRO",marca.getRubro().getId());
        if(marca.getSubRubro() != null)
            values.put("ID_SUBRUBRO",marca.getSubRubro().getId());
        int result = db.update("MARCA",values,strFilter,null);
        db.close();
        return result;
    }

    public HashMap<String, Marca> getHashMap() {
        return hashMap;
    }

    public void destroy(){

        instance = null;
    }
}

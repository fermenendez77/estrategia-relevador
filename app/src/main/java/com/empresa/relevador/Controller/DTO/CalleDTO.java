package com.empresa.relevador.Controller.DTO;

import com.empresa.relevador.Model.Calle;

import java.util.List;

/**
 * Created by fermenendez on 16/10/15.
 */
public class CalleDTO {

    private List<Calle> objects;

    public List<Calle> getObjects() {
        return objects;
    }

    public void setObjects(List<Calle> objects) {
        this.objects = objects;
    }
}

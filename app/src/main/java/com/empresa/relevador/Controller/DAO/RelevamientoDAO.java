package com.empresa.relevador.Controller.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.empresa.relevador.Controller.BO.CalleBO;
import com.empresa.relevador.Model.Lugar;
import com.empresa.relevador.Model.Marca;
import com.empresa.relevador.Model.Paridad;
import com.empresa.relevador.Model.Relevamiento;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by fermenendez on 5/9/15.
 */
public class RelevamientoDAO {

    //Singleton
    static private RelevamientoDAO instance = null;
    private Context mContext;
    private CalleBO calleBO;
    private LugarDAO lugarDAO;
    private HashMap<Integer,Lugar> hashLugar;
    private SQLiteHelper dbHelper;
    private MarcaDAO marcaDAO;
    private RelevamientoDAO(Context context) {


        this.mContext = context;
        this.dbHelper = new SQLiteHelper(context,"DB",null,DataBase.DATABASE_VERSION);
        hashLugar = new HashMap<Integer,Lugar>();
        calleBO = CalleBO.getInstance(context);
        lugarDAO = LugarDAO.getInstance(context);
        marcaDAO = MarcaDAO.getInstance(context);
        for(Lugar lugar : lugarDAO.getAll()){

            hashLugar.put(lugar.getId(),lugar);
        }
    }
    static public RelevamientoDAO getInstance(Context context){
        if(instance==null){
            instance = new RelevamientoDAO(context);
        }

        return instance;
    }

    public Relevamiento findById(long id){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM RELEVAMIENTO WHERE _ID = " + id, null);
            if (c.moveToFirst()) {
                Relevamiento relevamiento = new Relevamiento();
                relevamiento.setId(c.getLong(0));
                relevamiento.setNumeracion(c.getInt(1));
                relevamiento.setParidad(c.getString(2).equals("P") ? Paridad.PAR : Paridad.IMPAR);
                relevamiento.setPosicion(c.getInt(3));
                relevamiento.setSync(c.getInt(4));
                relevamiento.setRemoteId(c.getLong(5));
                relevamiento.setFecha(c.getString(6));
                relevamiento.setCalle(calleBO.findById(c.getInt(7))); //7
                relevamiento.setLugar(hashLugar.get(c.getInt(8)));
                if (hashLugar.get(c.getInt(8)).getTipo().equals("Local Ocupado")) {

                    Marca marca = marcaDAO.findbyId(c.getInt(9));
                    relevamiento.setMarca(marca);
                }

                relevamiento.setActivo(c.getInt(10));
                relevamiento.setOrientacion(c.getString(11));
                db.close();
                return relevamiento;
            } else
                db.close();
            return null;
        }finally {

            if(c!=null)
                c.close();
        }
    }

    public Relevamiento findByRemoteId(long remoteId){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM RELEVAMIENTO WHERE REMOTE_ID = " + remoteId, null);
            if (c.moveToFirst()) {
                Relevamiento relevamiento = new Relevamiento();
                relevamiento.setId(c.getLong(0));
                relevamiento.setNumeracion(c.getInt(1));
                relevamiento.setParidad(c.getString(2).equals("P") ? Paridad.PAR : Paridad.IMPAR);
                relevamiento.setPosicion(c.getInt(3));
                relevamiento.setSync(c.getInt(4));
                relevamiento.setRemoteId(c.getLong(5));
                relevamiento.setFecha(c.getString(6));
                relevamiento.setCalle(calleBO.findById(c.getInt(7))); //7
                relevamiento.setLugar(hashLugar.get(c.getInt(8)));
                if (hashLugar.get(c.getInt(8)).getTipo().equals("Local Ocupado")) {

                    Marca marca = marcaDAO.findbyId(c.getInt(9));
                    relevamiento.setMarca(marca);
                }

                relevamiento.setActivo(c.getInt(10));
                relevamiento.setOrientacion(c.getString(11));
                db.close();
                return relevamiento;
            } else
                db.close();
            return null;
        }finally {

            if(c!=null)
                c.close();
        }
    }

    public void revertDescendentes(){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT COUNT(_ID),PARIDAD,ID_CALLE,NUMERACION FROM RELEVAMIENTO WHERE ORIENTACION = 'Descendente' GROUP BY ID_CALLE,PARIDAD,NUMERACION"
                    , null);
            if (c.moveToFirst()) {
                do {

                    Integer cant = c.getInt(0);
                    Paridad paridad = (c.getInt(1)==2)?Paridad.PAR:Paridad.IMPAR;
                    int idCalle = c.getInt(2);
                    int numeracion = c.getInt(3);
                    Log.d("Descendentes",cant + "|" + paridad + "|" + idCalle + "|" + numeracion);
                    List<Relevamiento> relevamientos = findByStreet(idCalle,numeracion,paridad);
                    for(Relevamiento relevamiento : relevamientos){

                        relevamiento.setPosicion(cant);
                        revert(relevamiento.getId(),relevamiento);
                        cant--;
                    }
                } while (c.moveToNext());
            }
        }
        finally {
            if(c!=null)
                c.close();
        }
        db.close();
        return;
    }

    public List<Relevamiento> findByStreet(Integer idCalle, Integer numCalle, Paridad paridad){

        Integer parity = (paridad == Paridad.PAR)?2:1;
        List<Relevamiento> relevamientoList = new ArrayList<Relevamiento>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM RELEVAMIENTO WHERE ID_CALLE = " + idCalle +
                    " AND NUMERACION = " + numCalle + " AND PARIDAD = " + parity + " AND ACTIVO = 1 ORDER BY POSICION", null);
            if (c.moveToFirst()) {
                do {

                    Relevamiento relevamiento = new Relevamiento();
                    relevamiento.setId(c.getLong(0));
                    relevamiento.setNumeracion(c.getInt(1));
                    relevamiento.setParidad(c.getString(2).equals("P") ? Paridad.PAR : Paridad.IMPAR);
                    relevamiento.setPosicion(c.getInt(3));
                    relevamiento.setSync(c.getInt(4));
                    relevamiento.setRemoteId(c.getLong(5));
                    relevamiento.setFecha(c.getString(6));
                    relevamiento.setCalle(calleBO.findById(idCalle)); //7
                    relevamiento.setLugar(hashLugar.get(c.getInt(8)));
                    if (hashLugar.get(c.getInt(8)).getTipo().equals("Local Ocupado")) {

                        Marca marca = marcaDAO.findbyId(c.getInt(9));
                        relevamiento.setMarca(marca);
                    } else {

                        relevamiento.setMarca(null);
                    }
                    relevamiento.setActivo(c.getInt(10));
                    relevamiento.setOrientacion(c.getString(11));
                    relevamientoList.add(relevamiento);
                } while (c.moveToNext());
            }
        }
        finally {
            if(c!=null)
                c.close();
        }
        db.close();
        return relevamientoList;
    }

    public long insert(Relevamiento relevamiento){

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("NUMERACION",relevamiento.getNumeracion());
        values.put("PARIDAD",(relevamiento.getParidad()==Paridad.IMPAR)?1:2);
        values.put("POSICION",relevamiento.getPosicion());
        values.put("SYNC",1);
        values.put("REMOTE_ID",0);
        values.put("FECHA",format.format(date));
        values.put("ORIENTACION",relevamiento.getOrientacion());
        values.put("ID_CALLE",relevamiento.getCalle().getId());
        values.put("ID_LUGAR",relevamiento.getLugar().getId());
        values.put("ACTIVO",1);
        if(relevamiento.getMarca() != null)
            values.put("ID_MARCA",relevamiento.getMarca().getId() );
        long id =db.insert("RELEVAMIENTO", null,values);
        db.close();
        return id;
    }

    public long insertNews(Relevamiento relevamiento){

            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("NUMERACION", relevamiento.getNumeracion());
            values.put("PARIDAD", (relevamiento.getParidad() == Paridad.IMPAR) ? 1 : 2);
            values.put("POSICION", relevamiento.getPosicion());
            values.put("SYNC", 0);
            values.put("REMOTE_ID", relevamiento.getId());
            values.put("FECHA", format.format(date));
            values.put("ORIENTACION", "Ascendente");
            values.put("ID_CALLE", relevamiento.getCalle().getId());
            values.put("ID_LUGAR", relevamiento.getLugar().getId());
            values.put("ACTIVO", 1);
            if (relevamiento.getMarca() != null)
                values.put("ID_MARCA", relevamiento.getMarca().getId());
            long id = db.insert("RELEVAMIENTO", null, values);
            db.close();
            return id;
    }

    public int delete(Relevamiento relevamiento){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int result = 0;
        if(relevamiento.getRemoteId() == 0)
            result =  db.delete("RELEVAMIENTO","_ID = " + relevamiento.getId(),null);
        else{

            relevamiento.setActivo(0);
            logicalDelete(relevamiento.getId(),relevamiento);
            result = 1;
        }
        return result;
    }

    public int logicalDelete(long id, Relevamiento newRelevamiento){

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String strFilter = "_ID = " + id;
        ContentValues args = new ContentValues();
        args.put("ACTIVO",0);
        int result = db.update("RELEVAMIENTO",args,strFilter,null);
        db.close();
        return result;
    }

    public int update(long id, Relevamiento newRelevamiento){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String strFilter = "_ID = " + id;
        ContentValues args = new ContentValues();
        args.put("ID_LUGAR",newRelevamiento.getLugar().getId());
        args.put("POSICION",newRelevamiento.getPosicion());
        args.put("FECHA",format.format(date));
        args.put("SYNC",1);
        if(newRelevamiento.getMarca()!=null)
            args.put("ID_MARCA",newRelevamiento.getMarca().getId());
        int result = db.update("RELEVAMIENTO",args,strFilter,null);
        db.close();
        return result;
    }

    public int revert(long id, Relevamiento newRelevamiento){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String strFilter = "_ID = " + id;
        ContentValues args = new ContentValues();
        args.put("POSICION",newRelevamiento.getPosicion());
        args.put("ORIENTACION","Ascendente");
        int result = db.update("RELEVAMIENTO",args,strFilter,null);
        db.close();
        return result;
    }

    public int remoteUpdate(long remoteId, Relevamiento newRelevamiento){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String strFilter = "REMOTE_ID = " + remoteId;
        ContentValues args = new ContentValues();
        args.put("ID_LUGAR",newRelevamiento.getLugar().getId());
        args.put("POSICION",newRelevamiento.getPosicion());
        args.put("FECHA",format.format(date));
        args.put("SYNC",0);
        args.put("ACTIVO",1);
        args.put("PARIDAD",(newRelevamiento.getParidad()==Paridad.IMPAR)?1:2);
        args.put("ORIENTACION","Ascendente");
        if(newRelevamiento.getMarca()!=null)
            args.put("ID_MARCA",newRelevamiento.getMarca().getId());
        int result = db.update("RELEVAMIENTO",args,strFilter,null);
        db.close();
        return result;
    }

    public int updateNews(long id, Relevamiento newRelevamiento){

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String strFilter = "_ID = " + id;
        ContentValues args = new ContentValues();
        args.put("SYNC",newRelevamiento.getSync());
        args.put("REMOTE_ID",newRelevamiento.getRemoteId());
        int result = db.update("RELEVAMIENTO",args,strFilter,null);
        db.close();
        return result;
    }

    public List<Relevamiento> findNewsRelevamientos(){


        List<Relevamiento> relevamientoList = new ArrayList<Relevamiento>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM RELEVAMIENTO WHERE SYNC = 1 AND REMOTE_ID = 0" , null);
            if (c.moveToFirst()) {
                do {

                    Relevamiento relevamiento = new Relevamiento();
                    relevamiento.setId(c.getLong(0));
                    relevamiento.setNumeracion(c.getInt(1));
                    relevamiento.setParidad(c.getInt(2) == 2 ? Paridad.PAR : Paridad.IMPAR);
                    relevamiento.setPosicion(c.getInt(3));
                    relevamiento.setSync(c.getInt(4));
                    relevamiento.setRemoteId(c.getLong(5));
                    relevamiento.setFecha(c.getString(6));
                    relevamiento.setCalle(calleBO.findById(c.getInt(7))); //7
                    relevamiento.setLugar(hashLugar.get(c.getInt(8)));
                    if (hashLugar.get(c.getInt(8)).getTipo().equals("Local Ocupado")) {

                        Marca marca = marcaDAO.findbyId(c.getInt(9));
                        relevamiento.setMarca(marca);
                    } else {

                        relevamiento.setMarca(null);
                    }

                    relevamiento.setActivo(c.getInt(10));
                    relevamiento.setOrientacion(c.getString(11));
                    relevamientoList.add(relevamiento);
                } while (c.moveToNext());
            }
        }
        finally {
            if(c!=null)
                c.close();
        }
        db.close();
        return relevamientoList;
    }

    public List<Relevamiento> findUpdatesRelevamientos(){


        List<Relevamiento> relevamientoList = new ArrayList<Relevamiento>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM RELEVAMIENTO WHERE SYNC = 1 AND REMOTE_ID != 0" , null);
            if (c.moveToFirst()) {
                do {

                    Relevamiento relevamiento = new Relevamiento();
                    relevamiento.setId(c.getLong(5));
                    relevamiento.setNumeracion(c.getInt(1));
                    relevamiento.setParidad(c.getInt(2) == 2 ? Paridad.PAR : Paridad.IMPAR);
                    relevamiento.setPosicion(c.getInt(3));
                    relevamiento.setSync(c.getInt(4));
                    relevamiento.setRemoteId(c.getLong(5));
                    relevamiento.setFecha(c.getString(6));
                    relevamiento.setCalle(calleBO.findById(c.getInt(7))); //7
                    relevamiento.setLugar(hashLugar.get(c.getInt(8)));
                    if (hashLugar.get(c.getInt(8)).getTipo().equals("Local Ocupado")) {

                        Marca marca = marcaDAO.findbyId(c.getInt(9));
                        relevamiento.setMarca(marca);
                    } else {

                        relevamiento.setMarca(null);
                    }

                    relevamiento.setActivo(c.getInt(10));
                    relevamiento.setOrientacion(c.getString(11));
                    relevamientoList.add(relevamiento);
                } while (c.moveToNext());
            }
        }
        finally {
            if(c!=null)
                c.close();
        }
        db.close();
        return relevamientoList;
    }

    public List<Relevamiento> findDeletes(){


        List<Relevamiento> relevamientoList = new ArrayList<Relevamiento>();
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = null;
        try {
            c = db.rawQuery("SELECT * FROM RELEVAMIENTO WHERE ACTIVO = 0" , null);
            if (c.moveToFirst()) {
                do {

                    Relevamiento relevamiento = new Relevamiento();
                    relevamiento.setId(c.getLong(5));
                    relevamiento.setNumeracion(c.getInt(1));
                    relevamiento.setParidad(c.getInt(2) == 2 ? Paridad.PAR : Paridad.IMPAR);
                    relevamiento.setPosicion(c.getInt(3));
                    relevamiento.setSync(c.getInt(4));
                    relevamiento.setRemoteId(c.getLong(5));
                    relevamiento.setFecha(c.getString(6));
                    relevamiento.setCalle(calleBO.findById(c.getInt(7))); //7
                    relevamiento.setLugar(hashLugar.get(c.getInt(8)));
                    if (hashLugar.get(c.getInt(8)).getTipo().equals("Local Ocupado")) {

                        Marca marca = marcaDAO.findbyId(c.getInt(9));
                        relevamiento.setMarca(marca);
                    } else {

                        relevamiento.setMarca(null);
                    }

                    relevamiento.setActivo(c.getInt(10));
                    relevamiento.setOrientacion(c.getString(11));
                    relevamientoList.add(relevamiento);
                } while (c.moveToNext());
            }
        }
        finally {
            if(c!=null)
                c.close();
        }
        db.close();
        return relevamientoList;
    }

    public void destroy(){

        instance = null;
    }

    public HashMap<Integer, Lugar> getHashLugar() {
        return hashLugar;
    }

    //SYNC=1 -> Para sincronizar
    //REMOTE_ID = 0 -> Nunca fue sincronizado
    //Activo = 1 Esta activo
}

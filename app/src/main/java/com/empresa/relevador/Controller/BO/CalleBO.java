package com.empresa.relevador.Controller.BO;

import android.content.Context;

import com.empresa.relevador.Controller.DAO.CalleDAO;
import com.empresa.relevador.Model.Calle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by fermenendez on 29/8/15.
 */
public class CalleBO {

    private CalleDAO calleDAO;
    private HashMap<String,Calle> mapCalle;
    private Context mContext;


    static private CalleBO instance = null;
    private CalleBO(Context context) {

        mapCalle = new HashMap<String,Calle>();
        calleDAO = CalleDAO.getInstance(context);
        mContext = context;
    }
    static public CalleBO getInstance(Context context){
        if(instance==null){
            instance = new CalleBO(context);
        }
        return instance;
    }

    public List<Calle> getAll(Context context){


        return calleDAO.getAll();
    }

    public String[] getAllArray(){

        List<String> nombres = new ArrayList<String>();
        if(mapCalle.isEmpty()) {
            calleDAO = CalleDAO.getInstance(this.mContext);
            List<Calle> calles = calleDAO.getAll();

            for (Calle calle : calles) {

                nombres.add(calle.getNombre());
                mapCalle.put(calle.getNombre(), calle);
            }
        }
        else{

            for(String key: mapCalle.keySet()){

                nombres.add(key);
            }
        }
        String[] calleArray = nombres.toArray(new String[nombres.size()]);
        return calleArray;

    }

    public Calle findById(Integer id){

        calleDAO = CalleDAO.getInstance(this.mContext);
        return calleDAO.findById(id);
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public HashMap<String, Calle> getMapCalle() {
        return mapCalle;
    }

}

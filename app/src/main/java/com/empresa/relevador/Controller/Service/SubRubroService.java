package com.empresa.relevador.Controller.Service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.empresa.relevador.Controller.DAO.CalleDAO;
import com.empresa.relevador.Controller.DAO.SubRubroDAO;
import com.empresa.relevador.Controller.DTO.CalleDTO;
import com.empresa.relevador.Controller.DTO.SubRubroDTO;
import com.empresa.relevador.Model.Calle;
import com.empresa.relevador.Model.SubRubro;

/**
 * Created by fermenendez on 22/10/15.
 */
public class SubRubroService extends AbstractService {

    private Context mContext;
    private SubRubroDAO subRubroDAO;
    private final String SUBRUBRO = "subrubro/";

    public SubRubroService(Context context){

        this.mContext = context;
        subRubroDAO = SubRubroDAO.getInstance(context);
    }

    public void getNews(String fechaCreacion){

        String PARAMS = "getNews?date="+fechaCreacion;
        VolleySingleton.getInstance(mContext).addToRequestQueue(
                new GsonRequest<SubRubroDTO>(
                        BASE_URL+SUBRUBRO+PARAMS,
                        SubRubroDTO.class,
                        null,
                        new Response.Listener<SubRubroDTO>() {
                            @Override
                            public void onResponse(SubRubroDTO response) {
                                int count = 0;
                                for (SubRubro subRubro : response.getObjects()) {

                                    subRubroDAO.insert(subRubro);
                                    count++;
                                }
                                Log.d("SubRubroService","Se insertaron "+ count + "SubRubros");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error", "Error Respuesta en JSON: " + error.getMessage());
                                return;
                            }
                        }
                ){

                    @Override
                    public Priority getPriority() {
                        return Priority.IMMEDIATE;
                    }
                }
        );
    }
    public void getUpdates(String fechaCreacion){

        String PARAMS = "getUpdates?date="+fechaCreacion;
        VolleySingleton.getInstance(mContext).addToRequestQueue(
                new GsonRequest<SubRubroDTO>(
                        BASE_URL+SUBRUBRO+PARAMS,
                        SubRubroDTO.class,
                        null,
                        new Response.Listener<SubRubroDTO>() {
                            @Override
                            public void onResponse(SubRubroDTO response) {
                                int count = 0;
                                for (SubRubro subRubro : response.getObjects()) {

                                    subRubroDAO.update(subRubro);
                                    count++;
                                }
                                Log.d("SubRubroService","Se actualizaron "+ count + "SubRubros");
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error", "Error Respuesta en JSON: " + error.getMessage());
                                return;
                            }
                        }
                ){

                    @Override
                    public Priority getPriority() {
                        return Priority.HIGH;
                    }
                }
        );
    }
}

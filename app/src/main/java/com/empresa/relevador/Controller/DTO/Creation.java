package com.empresa.relevador.Controller.DTO;

/**
 * Created by fermenendez on 26/10/15.
 */
public class Creation {

    private Integer id;
    private String creationDate;

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }
}

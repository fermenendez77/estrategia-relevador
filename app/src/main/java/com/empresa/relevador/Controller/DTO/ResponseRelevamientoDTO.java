package com.empresa.relevador.Controller.DTO;

/**
 * Created by fermenendez on 27/10/15.
 */
public class ResponseRelevamientoDTO {

    public ResponseRelevamientoDTO(){


    }
    public ResponseRelevamientoDTO(String status, long remoteID, long id) {

        this.status = status;
        this.remoteID = remoteID;
        this.id = id;
    }
    private String status;
    private long remoteID;
    private long id;
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public long getRemoteID() {
        return remoteID;
    }
    public void setRemoteID(long remoteID) {
        this.remoteID = remoteID;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }


}
